<?php

require_once(__DIR__."/class-ROSFUpdatesQueue.php");

/**
 * Fired during plugin activation
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/includes
 * @author     Virtual Bit <info@virtualbit.it>
 */
class Ro_Shared_Folders_Activator {


	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		self::scheduleQueueUpdates();
	}

	public static function scheduleQueueUpdates() {
		/*
		ROSFUpdatesQueue::initCronHook();

		if (wp_next_scheduled(ROSFUpdatesQueue::$wpcronHookName) === false) {
			wp_schedule_event( time(), ROSFUpdatesQueue::$wpcronInterval, ROSFUpdatesQueue::$wpcronHookName );
		}
		*/
	  
	}
}
