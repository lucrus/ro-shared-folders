<?php

require_once(__DIR__ . '/class-ROSFSync.php');
require_once(__DIR__ . '/class-ROSFThumbs.php');
require_once(__DIR__ . '/class-ROSFPermissions.php');

class ROSF_DirectoryDescriptor
{
  public string $curdir;
  public string $subdir;
  public int $depth;
  public int $nFiles;
  public bool $hot;
  public array $subtree;
  public ROSF_DirectoryDescriptor $parent;

  function __toString()
  {
    return $this->subdir;
  }
}

class ROSF_FileDescriptor
{
  public string $name;
  public bool $hot;
  public int $mtime;
  public string $parentDir;
}

class ROSF_FSCommon extends ROSFPermissions
{
  private static $directDownloadLink;

  private static ?ROSF_DirectoryDescriptor $root = null;

  public static function _listOnlyFiles(string $sandbox, string $dir)
  {
    $files = [];
    if (ROSFSync::currentUserCanAccess($dir)) {
      $nwyt = self::_getNWYTTime();
      foreach (scandir($sandbox . $dir) as $subfile) {
        $curfile = $dir . "/" . $subfile;
        if (is_file($sandbox . $curfile) &&
            !self::shouldSkip($subfile)) {
          $fd = new ROSF_FileDescriptor();
          $fd->name = $subfile;
          $fd->hot = self::_isNew($sandbox, $dir, $subfile, $nwyt);
          $fd->mtime = filemtime($sandbox . $curfile);
          $fd->parentDir = $dir;
          array_push($files, $fd);
        }
      }
    }
    return $files;
  }

  public static function shouldSkip($file) {
    $lfile = strtolower($file);
    return $lfile == "thumbs.db" ||
            $lfile == "desktop.ini" ||
            $lfile == ".ds_store" ||
            $lfile == "__macosx" ||
            $lfile == "." ||
            $lfile == "..";
  }

  protected static function _listOnlyDirectories(string $sandbox, string $basedir, $onlyAtDepth = -1, $curdepth = 0, ROSF_DirectoryDescriptor $parent = null)
  {
    $check_caps_for_depths_lte = count(Ro_Shared_Folders_Admin::getTaxonomyNames()) - 1;
    if (!empty($basedir) && strpos($basedir, '/') !== 0) {
      $basedir = '/' . $basedir;
    }
    $result = [];
    $nwyt = self::_getNWYTTime();
    foreach (scandir($sandbox . $basedir) as $subdir) {
      if (self::shouldSkip($subdir)) {
        continue;
      }

      $lcdirname = strtolower(basename($subdir));
      $protectedDirs = Ro_Shared_Folders_Admin::getProtectedFolders();
      $protected = in_array($lcdirname, $protectedDirs);
  
      $curdir = $basedir . "/" . $subdir;
      if (
          is_dir($sandbox . '/' . $curdir) &&
            ( ($protected && ROSFSync::currentUserCanAccess($curdir))
                            || 
              (!$protected && ($curdepth > $check_caps_for_depths_lte ||
                            ROSFSync::currentUserCanAccess($curdir)))
            ) 
        ) {
        $descriptor = new ROSF_DirectoryDescriptor();
        $descriptor->curdir = $curdir;
        $descriptor->subdir = $subdir;
        $descriptor->depth = $curdepth;
        $descriptor->parent = $parent ?? self::getRootDirectory($curdepth === 0 ? $descriptor : false);
        $descriptor->nFiles = sizeof(self::_listOnlyFiles($sandbox, $curdir));
        $descriptor->hot = self::_isNew($sandbox, $curdir, '.', $nwyt);
        if ($onlyAtDepth === -1) {
          $descriptor->subtree = self::_listOnlyDirectories($sandbox, $curdir, $onlyAtDepth, $curdepth + 1, $descriptor);
          array_push($result, $descriptor);
        } else if ($onlyAtDepth === $curdepth) {
          array_push($result, $descriptor);
        } else if ($curdepth < $onlyAtDepth) {
          $sub = self::_listOnlyDirectories($sandbox, $curdir, $onlyAtDepth, $curdepth + 1, $descriptor);
          array_push($result, ...$sub);
        }
      }
    }
    $result = array_unique($result);
    asort($result, SORT_STRING);
    if ($curdepth === 0) {
      self::fixHots($result);
    }
    return $result;
  }

  private static function getRootDirectory($sub = false) {
    if (self::$root === null) {
      self::$root = new ROSF_DirectoryDescriptor();
      self::$root->curdir = "/";
      self::$root->subdir = "/";
      self::$root->nFiles = 0;
      self::$root->depth = 0;
      self::$root->parent = self::$root;
      self::$root->subtree = [];
    }
    if ($sub && !in_array($sub, self::$root->subtree)) {
      array_push(self::$root->subtree, $sub); 
    }
    return self::$root;
  }

  private static function fixHots(Array $dirs) {
    foreach ($dirs as $dir) {
      $dir->hot = $dir->hot || self::containsHotDescriptors($dir);
      if ($dir->hot && !empty($dir->subtree)) {
        self::fixHots($dir->subtree);
      }
    }
  }

  private static function containsHotDescriptors(ROSF_DirectoryDescriptor $dir) {
    $result = $dir->hot;
    if (!$result && !empty($dir->subtree)) {
      for ($i = 0; $i < count($dir->subtree) && !$result; $i++) {
        $result = $result || self::containsHotDescriptors($dir->subtree[$i]);
      }
    }
    return $result;
  }

  private static function findHotsRecursive(array $dirs, string $sandbox, bool $returnDirectories = false)
  {
    $hotfiles = [];
    foreach ($dirs as $dir) {
      if ($dir->hot) {
        if (!empty($dir->subtree)) {
          $subhots = self::findHotsRecursive($dir->subtree, $sandbox, $returnDirectories);
          array_push($hotfiles, ...$subhots);
        }
        $allfiles = self::_listOnlyFiles($sandbox, $dir->curdir);
        foreach ($allfiles as $file) {
          if ($file->hot) {
            if ($returnDirectories) {
              array_push($hotfiles, $dir);
              break;
            } else {
              array_push($hotfiles, $file);
            }
          }
        }
      }
    }
    return $hotfiles;
  }

  public static function findHottestFiles(string $sandbox, int $limit = PHP_INT_MAX) {
    return self::findHottest($sandbox, $limit, false);
  }

  public static function findHottestDirs(string $sandbox, int $limit = PHP_INT_MAX) {
    return self::findHottest($sandbox, $limit, true);
  }

  private static function findHottest(string $sandbox, int $limit, $returnDirectories = false)
  {
    $result = [];
    $dirs = self::_listOnlyDirectories($sandbox, '');
    $hots = self::findHotsRecursive($dirs, $sandbox, $returnDirectories);
    usort($hots, function ($a, $b) {
      return $b->mtime - $a->mtime;
    });
    if ($limit > count($hots))
      $limit = count($hots);
    $result = array_splice($hots, 0, $limit);

    return $result;
  }

  protected static function _getNWYTTime()
  {
    $NWYToption = get_option(Ro_Shared_Folders_Admin::$newWhenYoungerThan, "7");
    $days = intval($NWYToption);
    $nwyt = time() - $days * 86400;
    return $nwyt;
  }

  /**
   * $file può essere una stringa o un'istanza di ROSF_FileDescriptor
   */
  protected static function _isNew(string $sandbox, string $dir, $file, int $nwyt)
  {
    $filename = '';
    if (is_string($file)) {
      $filename = $file;
      $mtime = filemtime($sandbox . $dir . '/' . $filename);
    } else {
      $filename = $file->name;
      $mtime = filemtime($sandbox . $dir . '/' . $filename);
      $file->mtime = $mtime;
    }
    return ($mtime > $nwyt) || self::_containsHotFile($sandbox, $dir, $filename, $nwyt);
  }

  private static function _containsHotFile($sandbox, $dir, $filename, $nwyt)
  {
    if ($filename !== '.') {
      return false;
    }

    if (!ROSFSync::currentUserCanAccess($dir)) {
      return false;
    }
    
    $files = self::_listOnlyFiles($sandbox, $dir);
    $result = false;
    foreach ($files as $file) {
      $result = $result || self::_isNew($sandbox, $dir, $file, $nwyt);
    }
    return $result;
  }

  public static function thumbname($thumbsdir, $dir, $file) {
    return $thumbsdir.$dir.'/'.$file.'.jpg';
  }

  public static function isFileNewerThan($src, $dst) {
    if (file_exists($src)) {
      if (!file_exists($dst)) {
        return true;
      }
      $mtimeSrc = filemtime($src);
      $mtimeTmb = filemtime($dst);
      return $mtimeSrc > $mtimeTmb;
    }
    return false;
  }

  public static function mkdir($dir) {
    if (!file_exists($dir)) {
      mkdir($dir, 0777, true);
    }
  }

  public static $sharingKeyDirParam = "dir";
  public static $sharingKeyFileParam = "file";

  public static function pathComponentToSharingKey(String $pc = null) {
    if (empty($pc)) {
      return '';
    }
    return self::base64url_encode($pc);
  } 

  public static function sharingKeyToPathComponent($key) {
    return self::base64url_decode($key);
  }

  public static function base64url_encode($data) {
    $bdata = base64_encode($data);
    $tourl = strtr($bdata, '+/', '-_');
    $nopadding = rtrim($tourl, '=');
    return $nopadding;
  }
  
  public static function base64url_decode($data) {
    $withpadding = str_pad($data, strlen($data) %4, '=', STR_PAD_RIGHT);
    $fromurl = strtr($withpadding, '-_', '+/');
    $rdata = base64_decode($fromurl); 
    return $rdata;
  }

	private static function getDirectDownloadLink(array $params) {
    if (empty(self::$directDownloadLink)) {      
      $ddpage = array(
        'post_status'           => 'publish', 
        'post_type'             => 'page',
        'post_name'             => Ro_Shared_Folders::$directdownloadpageslug,
        'post_author'           => get_current_user_id(),
        'post_content'          => '',
        'post_title'            => Ro_Shared_Folders::$directdownloadpageslug,
        'menu_order'            => 30          
        );
      self::$directDownloadLink = get_post_permalink(self::create_or_update_page($ddpage));
    }
    if (strpos(self::$directDownloadLink, '?') > 0)
      return self::$directDownloadLink.'&'.http_build_query($params);
    else
      return self::$directDownloadLink.'?'.http_build_query($params);
	}

	public static function create_or_update_page($params)
	{
	  $page = get_page_by_title($params['post_title']);
	  if ($page != NULL) {
      $params['ID'] = $page->ID;
		  return wp_update_post($params);
	  }
	  else
		  return wp_insert_post($params);
	}

  public static function getDirectoryNamePermalink(string $dir) {
    $params = array(self::$sharingKeyDirParam => self::pathComponentToSharingKey($dir));
    return self::getDirectDownloadLink($params);
  }

  public static function getDirectoryPermalink(ROSF_DirectoryDescriptor $dir) {
    return self::getDirectoryNamePermalink($dir->curdir);
  }

  public static function getFileNamePermalink(string $parentDir, string $filename) {
    $params = array(self::$sharingKeyDirParam => self::pathComponentToSharingKey($parentDir), 
                    self::$sharingKeyFileParam => self::pathComponentToSharingKey($filename));
    return self::getDirectDownloadLink($params);
  }

  public static function getFilePermalink(ROSF_FileDescriptor $file) {
    return self::getFileNamePermalink($file->parentDir, $file->name);
  }

  public static function getSharingHtml() {
    $result = '<div class="rosf-sharing-lightbox">';
      $result .= '<div class="rosf-sharing-dialog">';
        $result .= '<div class="rosf-sharing-dialog-content">';
          $result .= '<h4>Condividi il link a questa directory</h4>';
          $result .= '<div class="rosf-sharing-dialog-link">';
            $result .= '<input id="rosf-link-target-container" readonly>';
          $result .= '</div>';
          $result .= '<span class="rosf-copytoclipboard">';
          $result .= '<div id="rosf-copied-notification">Copiato</div>';
        $result .= '</div>';
      $result .= '</div>';
    $result .= '</div>';
    return $result;
  }
}
