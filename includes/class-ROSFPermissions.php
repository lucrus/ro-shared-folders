<?php

require_once(__DIR__."/class-ROSFUpdatesQueue.php");
require_once(__DIR__."/class-ROSF_FSCommon.php");

class ROSFPermissions {

  public static $role_admin = "rosf_role_admin"; 
  public static $rosf_base_cap = "rosf_cap_read";
  public static $deepcap_suffix = "_*";
  public static $login_autoredirect_id = "rosf_login_autoredirect";

  public static array $simulatedCapabilities = [];

  public static function isAdminUser() {
    if (count(self::$simulatedCapabilities) > 0)
      return false;
    $user = wp_get_current_user();
    $allowed_roles = array('administrator', self::$role_admin);
    return array_intersect( $allowed_roles, $user->roles );
  }

  public static function getDirectoryCapabilityName($dir) {
    if (strpos($dir, "/") === 0) {
      $dir = substr($dir, 1);
    }
    $dir = str_replace("_", "", $dir);
    $dir = str_replace("/", "_", $dir);
    return self::$rosf_base_cap."_".$dir;
  }

  public static function getDirectoryDeepCapabilityName($dir) {
    $cap = self::getDirectoryCapabilityName($dir);
    return $cap.self::$deepcap_suffix;
  }

  public static function isDeepCapability(string $capname) {
    // Poor man's implementation of str_ends_with for PHP <= 7.x
    return (strrpos($capname, self::$deepcap_suffix) + strlen(self::$deepcap_suffix) === strlen($capname));
  }

  public static function getPlainCapabilityNameFromDeep(string $deepcapname) {
    if (self::isDeepCapability($deepcapname)) {
      return substr($deepcapname, 0, strlen($deepcapname) - strlen(self::$deepcap_suffix));
    }
    return $deepcapname;
  }

  public static function remove_caps($role, Array $caps ) {
    $wprthis = wp_roles();
    if ( ! isset( $wprthis->roles[ $role ] ) ) {
        return;
    }
 
    foreach ($caps as $cap)
      unset($wprthis->roles[ $role ]['capabilities'][ $cap ] );

    if ( $wprthis->use_db ) {
        update_option( $wprthis->role_key, $wprthis->roles );
    }
  }

  private static function currentUserHasDeepCapOnParent($dir, array $protectedDirs) {
    $parent = dirname($dir);
    $lcparentName = strtolower(basename($parent));
    $parentProtected = in_array($lcparentName, $protectedDirs);
    $hasDeepOnParent = ((!$parentProtected) && self::currentUserCan(self::getDirectoryDeepCapabilityName($parent))) || 
                        ($parentProtected && self::currentUserCan(self::getDirectoryDeepCapabilityName($lcparentName)));
    if ($hasDeepOnParent || empty($parent))
      return $hasDeepOnParent;
    if ($parent == $dir) // raggiunta la root dir senza trovare la capability possiamo dire false 
      return false;
    return self::currentUserHasDeepCapOnParent(dirname($dir), $protectedDirs);
  }

  public static function currentUserCanAccess($dir) {
    $lcdirname = strtolower(basename($dir));
    $protectedDirs = Ro_Shared_Folders_Admin::getProtectedFolders();
    $protected = in_array($lcdirname, $protectedDirs);
    return self::isAdminUser() ||
        self::currentUserCan(self::getDirectoryCapabilityName($dir)) ||
        self::currentUserCan(self::getDirectoryDeepCapabilityName($dir)) ||
        ($protected && self::currentUserCan(self::getDirectoryCapabilityName($lcdirname))) ||
        ((!$protected) && self::currentUserHasDeepCapOnParent($dir, $protectedDirs)) ||
        ($protected && self::currentUserCan(self::getTPLCapabilityName($dir, $lcdirname)));
    }

  public static function currentUserCan($capability) {
    if (count(self::$simulatedCapabilities) === 0)
      return current_user_can($capability);
    return in_array($capability, self::$simulatedCapabilities, true);
  }

  /** TPL = Taxonomy Protection Level */
  public static function getTPLCapabilityName($curdir, $protecteddir) {
    $level = get_option(Ro_Shared_Folders_Admin::$taxonomyProtectionLevel, false);
    if (!empty($level)) {
      $dirlevel = intval($level) - 1;
      if (strpos($curdir, '/') === 0) {
        $curdir = substr($curdir, 1);
      }
      $dirs = explode("/", $curdir);
      if (count($dirs) > $dirlevel) {
        $similglob = '*'.$dirs[$dirlevel].'*'.$protecteddir;
        return self::getDirectoryCapabilityName($similglob);
      } 
    }
    
  }

  public static function sanitize_path($basedir) {

    if (!empty($basedir)) {
      // We don't want any backslashes. If users needed
      // those to escape double quotes or the like, they'll have to 
      // remove double quotes from their files and directory names,
      // because we try to prioritize security over anything fancy.
      // And we don't give a fsck if this doesn't work on Windows
      // servers, because we do not support Windows on the server.
      // In other words the shared folder cannot contain any files
      // whose names contain double quotes, backslashes or any other
      // characters that need to be escaped with backslashes. Moreover,
      // the basedir path cannot contain backslashes, which pretty much
      // excludes Windows servers.
      // Maybe this is not even necessary from a security point of view,
      // but we better play it safe and don't allow fancy not typeable
      // sh*t in the files names.
      if (strpos($basedir, "\\") !== false) {
          $basedir = str_replace("\\", "/", $basedir);
      }

      // let's strip all the ".." references so that no one can
      // trivially escape the sandbox 
      $basedir = str_replace("..", "", $basedir);

      // and let's strip double slashes the previous replace may
      // have left behind
      while (strpos($basedir, "//") !== false)
        $basedir = str_replace("//", "/", $basedir);

      // let's strip leading slashes too
      while (strpos($basedir, "/") === 0) {
          $basedir = substr($basedir, 1);
      }
    }

    if (empty($basedir)) {
      // if no basedir was specified, or if
      // the computations above left basedir empty, just show the
      // root of the sandbox

      $basedir = '';
    }

    return $basedir;
  }

  public static function getUtentiConCaps(array $caps, array $users) {
    $cappedUsers = array();

    foreach($users as $user) {
    
      $all = true;
      foreach($caps as $cap) {
        if(!$user->has_cap($cap)){
          $all = false;
          break;
        }
      }

      if ($all) {
        $cappedUsers[] = $user;
      }
    }
    return $cappedUsers;
  }

  public static function calcolaTutteLeCombinazioni(array $elementi) {
    $result = [];
    for ($i = count($elementi); $i > 0; $i--) {
      foreach(new ROSFCombinations($elementi, $i) as $comb) {
        $result[] = $comb;
      }
    }
    return $result;
  }
}

class ROSFCombinations implements Iterator
{
    protected $c = null;
    protected $s = null;
    protected $n = 0;
    protected $k = 0;
    protected $pos = 0;

    function __construct($s, $k) {
        if(is_array($s)) {
            $this->s = array_values($s);
            $this->n = count($this->s);
        } else {
            $this->s = (string) $s;
            $this->n = strlen($this->s);
        }
        $this->k = $k;
        $this->rewind();
    }
    function key() {
        return $this->pos;
    }
    function current() {
        $r = array();
        for($i = 0; $i < $this->k; $i++)
            $r[] = $this->s[$this->c[$i]];
        return is_array($this->s) ? $r : implode('', $r);
    }
    function next(): void {
        if($this->_next())
            $this->pos++;
        else
            $this->pos = -1;
    }
    function rewind(): void {
        $this->c = range(0, $this->k);
        $this->pos = 0;
    }
    function valid(): bool {
        return $this->pos >= 0;
    }

    protected function _next() {
        $i = $this->k - 1;
        while ($i >= 0 && $this->c[$i] == $this->n - $this->k + $i)
            $i--;
        if($i < 0)
            return false;
        $this->c[$i]++;
        while($i++ < $this->k - 1)
            $this->c[$i] = $this->c[$i - 1] + 1;
        return true;
    }
}