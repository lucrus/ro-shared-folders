<?php

require_once(__DIR__."/class-ROSFUpdatesQueue.php");
require_once(__DIR__."/class-ROSF_FSCommon.php");

class ROSFSync extends ROSF_FSCommon {
  
  public static function updatePost(string $sandbox, string $relativepath, string $file, array $tags, array $taxonomies) {
    $filePost = array(
      'post_title'    => wp_strip_all_tags($file),
      'post_content'  => '',
      'post_status'   => 'publish',
      'post_author'   => 1,
      'post_type'     => ROSFile_MetaKeys::$type_slug,
      'meta_input'    => ["rosf_file_id" => $relativepath.'/'.$file, "rosf_sandbox" => $sandbox]
    );

    $meta_input = $filePost['meta_input'];
    $custom_field_key = "rosf_file_id";
    $relativepath = $meta_input[$custom_field_key];

    $query = self::getPostsByPathAndFileName($relativepath);
    if (count($query->posts) === 0) {
      $postid = wp_insert_post($filePost);
      for ($i = 0; $i < count($taxonomies); $i++) {
        wp_set_post_terms($postid, $tags[$i], $taxonomies[$i], $i > 0);
      }
    }
  }
  
  public static function getPostsByPathAndFileName($fpafn) {
    if (strncmp('/', $fpafn, 1) === 0)
      $fpafn = substr($fpafn, 1);
    $custom_field_key = "rosf_file_id";
    $args = array(
      'post_type'  => ROSFile_MetaKeys::$type_slug,
      'meta_key'   => $custom_field_key,
      'meta_query' => array(
          array(
              'key'     => $custom_field_key,
              'value'   => $fpafn,
              'compare' => '=',
          ),
      ),
    );
    return new WP_Query( $args );
  } 

  public static function insertTerm($curdepth, $taxonomies, $subdir, $relativeDir) {
    $realname = $subdir;
    $subdir = strtr($realname, ",", "-"); // we can't allow commas in the taxonomy items names, because we use commas in URL params to separate them in a list
    $res = wp_insert_term($subdir, $taxonomies[$curdepth], ["description" => $relativeDir]);
    if (is_array($res)) {
      update_term_meta($res['term_id'], ROSFile::$visibleNameFieldPrefix.$taxonomies[$curdepth], $realname);
    } else {
      // questo term esiste già, ma quello esistente potrebbe corrispondere ad una directory che si chiama nello 
      // stesso modo di quella attuale, ma si trova in un altro punto dell'albero, alla stessa profondità 
      // (quindi qui messo nella stessa tassonomia), o ad una profondità diversa (quindi in altra tassonomia),
      // ma, in ogni caso, con un parent differente da quello del term che qui si cercava di inserire 
      // (altrimenti, se avesse lo stesso parent folder, sarebbe anche lo stesso identico term che è già stato
      // inserito e qui non dovermmo fare nulla). Devo quindi recuperare l'altro term esistente fra le varie
      // tassonomie possibili e, quando lo trovo, verificare se la sua descrizione contenga già $relativeDir.
      // Se non la contiene, aggiungerla.
      for ($i = 0; $i < count($taxonomies); $i++) {
        $existingTerm = get_term_by('name', $subdir, $taxonomies[$i]);
        if ($existingTerm !== false) {
          // ho trovato il term esistente, ora devo verificare se contiene 
          // già la $relativeDir nella sua description
          $decoded_description = html_entity_decode($existingTerm->description);
          if (strpos($decoded_description, $relativeDir) === false) {
            // non la contiene, quindi la devo aggiornare
            $existingTerm->description .= '||' . $relativeDir;
            $res = wp_update_term($existingTerm->term_id, $taxonomies[$i], [
              "description" => $existingTerm->description,
              "slug" => $existingTerm->slug]);
            if (is_array($res)) {
              update_term_meta($res['term_id'], ROSFile::$visibleNameFieldPrefix.$taxonomies[$curdepth], $realname);
            }
          }
        }
      }
    }
  }

  /**
   * @param string $subdir è il nome della cartella senza parent
   * @param string $relativeDir è il nome della cartella con tutto il path relativo a partire dalla sandbox
   * Se la subdir non è una delle protette, allora la capability dovrà includere tutto il path relativo.
   * Se la subdir è una delle protette, allora la capability dovrà includere solo il nome della subdir
   */
  public static function getCapFromSubdir($subdir, $relativeDir, $protectedDirs) {
    $cap_subdir = strtolower($subdir);
    if (!in_array($cap_subdir, $protectedDirs)) {
      $cap_subdir = $relativeDir;
    } 
    return $cap_subdir;
  }

  /**
   * crea gli items, corrispondenti alle cartelle dell'area riservata, da mettere nelle rispettive 
   * tassonomie, corrispondenti al livello delle stesse cartelle.
   * Crea anche i post corrispondenti ai files nelle cartelle.
   * Restituisce un array con le capabilites, tenendo conto dell'impostazione "Protected Folders".
   * TODO FIXME KNOWN BUG: le capabilites delle cartelle protette con taxonomy protection level
   *   sono generate a partire dai terms presenti nella tassonomia specificata dal livello, ma
   *   tale generazione viene eseguita come prima fase della ricorsione, invece che come ultima, di 
   *   conseguenza utlizza i terms generati da una precedente sincronizzazione.
   */
  public static function recursiveScanDirectory(string $sandboxdir, 
                                                array $taxonomies,
                                                string $base = '', 
                                                string $prefix = '', 
                                                array $parents = [],
                                                array $protectedDirs = NULL) {
    $result = [];
    $postsCount = 0;
    if (NULL === $protectedDirs) {
      $taxprotlevel = intval(get_option(Ro_Shared_Folders_Admin::$taxonomyProtectionLevel, 0));
      $proterms = array();
      if ($taxprotlevel > 0) {
        $protax = $taxonomies[$taxprotlevel - 1]; // 1 based in the UI, 0 based in the array
        $proterms = get_terms(['taxonomy' => $protax]);
      }
      $protectedDirs = Ro_Shared_Folders_Admin::getProtectedFolders();
      foreach ($protectedDirs as $pd) {
        $pcap = self::getDirectoryCapabilityName($pd);
        $pdcap = self::getDirectoryDeepCapabilityName($pd);
        array_push($result, $pcap, $pdcap);
        foreach ($proterms as $term) {
          $similglob = '*'.$term->name.'*'.$pd;
          $pcap = self::getDirectoryCapabilityName($similglob);
          $pdcap = self::getDirectoryDeepCapabilityName($similglob);
          array_push($result, $pcap, $pdcap);
        }
      }
    }
    $curdepth = sizeof($parents);
    $maxdepth = sizeof($taxonomies);
    if (empty($base))
      $base = $sandboxdir;
    foreach (scandir($base) as $subdir) {
      if (self::shouldSkip($subdir)) {
        continue;
      }
      $relativeDir = $subdir;
      if (!empty($prefix))
        $relativeDir = $prefix.'/'.$relativeDir;
      if (is_dir($sandboxdir.'/'.$relativeDir)) {
        $cap_subdir = self::getCapFromSubdir($subdir, $relativeDir, $protectedDirs);
        if ($curdepth < $maxdepth - 1) {
          $cap = self::getDirectoryCapabilityName($cap_subdir);
          array_push($result, $cap);
        }
        if ($curdepth < $maxdepth) {
          self::insertTerm($curdepth, $taxonomies, $subdir, $relativeDir);
          $deepcap = self::getDirectoryDeepCapabilityName($cap_subdir);
          array_push($result, $deepcap);
        }
        $copy_of_parents = $parents;
        array_push($copy_of_parents, $subdir);
        $subres = self::recursiveScanDirectory($sandboxdir, 
                                                $taxonomies,  
                                                $sandboxdir.'/'.$relativeDir, 
                                                $relativeDir, $copy_of_parents, $protectedDirs);

        $result = array_merge($result, $subres['caps']);
        $postsCount += $subres['postsCount'];
      } else { // it's a file, not a directory
        // we have to create a post and attach $curdepth terms, one for each taxonomy,
        // except when $curdepth is equal or greater than $maxdepth, and in that case
        // we attach $maxdepth terms instead.
        $filename = $subdir;
        self::updatePost($sandboxdir, $prefix, $filename, $parents, $taxonomies);
        $postsCount++;
      }
    }
    return ["caps" => $result, "postsCount" => $postsCount];
  }
  
  public static function deleteTaxTerm($args) {
    $termid = $args[0];
    $tax = $args[1];
    wp_delete_term($termid, $tax); 
  }

  public static function get_delete_all_taxonomy_terms_steps($tax) {
    $terms = get_terms( array(
      'taxonomy' => $tax,
      'hide_empty' => false
    ));

    $steps = [];
    if (!($terms instanceof WP_Error)) {
      foreach ( $terms as $term ) {
        $step = new ROSFUpdateStep();
        $step->payload = [$term->term_id, $tax];
        $step->updaterFunction = array('ROSFSync', 'deleteTaxTerm');
        array_push($steps, $step);
      }
    }

    return $steps;
  }

  public static function deleteOldTaxonomiesTerms($oldTaxonomies, $newTaxonomies) {
    $otax = explode(',', $oldTaxonomies);

    $steps = [];
    foreach ($otax as $tax) {
      array_push($steps, ...(self::get_delete_all_taxonomy_terms_steps($tax)));
    }
    ROSFUpdatesQueue::batchExec($steps);
  }

  public function sync($args) {
    self::staticSync($args);
  }

  public static function staticSync($args) {
    global $wp_roles;

    $dir = get_option(Ro_Shared_Folders_Admin::$baseDir);
    if (empty($dir)) return;

    $taxonomies = Ro_Shared_Folders_Admin::getTaxonomyNames();

    // eseguo una prima volta per aggirare in known bug documentato
    // nel commento della funzione
    self::recursiveScanDirectory($dir, $taxonomies);

    // eseguo la seconda volta e questa volta uso il risultato
    $allcaps = self::recursiveScanDirectory($dir, $taxonomies);

    array_unshift($allcaps['caps'], self::$rosf_base_cap);

    $wproles = wp_roles();
    
    $wproles->use_db = true;
    $rolename = self::$role_admin;
    $arole = get_role($rolename);
    if (!empty($arole)) {
      foreach($arole->capabilities as $ck => $cv) {
        if (in_array($ck, $allcaps['caps'])) {
          unset($allcaps['caps'][$ck]); // c'è già, non devo aggiungerla dopo
        } else {
          foreach (array_keys($wp_roles->roles) as $role) {
            $wp_roles->remove_cap($role, $ck);
          }
          $arole->remove_cap($ck);            
        }
      }
    } else {
      $arole = add_role($rolename, "Shared Folders Admin");
    }

    foreach ($allcaps["caps"] as $cap) {
      $arole->add_cap($cap);
    }

    $deleted = self::deleteOldPosts($dir);

		if ( class_exists( 'WP_CLI' ) ) {
      $mesg = count($allcaps['caps'])." capabilities and ".$allcaps['postsCount']." files sync'd.";
      $mesg .= "\n\n".count($deleted)." files were not found anymore, so corresponding posts were deleted:\n\n";
      $mesg .= implode("\n", $deleted);
			WP_CLI::success($mesg);
		}		
    
  }

  /**
   * fa il giro su tutti i post di tipo ROSFile_MetaKeys::$type_slug e
   * verifica che effettivamente esista il file corrispondente nell'area
   * riservata. Se non esiste, elimina il post.
   */
  public static function deleteOldPosts($baseDir) {
    $posts = get_posts([
      'post_type' => ROSFile_MetaKeys::$type_slug,
      'post_status' => 'publish',
      'numberposts' => -1
    ]);

    $deleted = [];

    foreach ($posts as $post) {
      $relativefilepath = get_post_meta($post->ID, 'rosf_file_id', true);
      $fullPath = $baseDir . '/'. $relativefilepath;
      if (!file_exists($fullPath)) {
        $originalBasedir = get_post_meta($post->ID, 'rosf_sandbox', true);
        $fullPath = $originalBasedir . '/'. $relativefilepath;
        if (!file_exists($fullPath)) {
          wp_delete_post($post->ID); // la prima volta che viene eseguita questa funzione
          // il post va nel cestino, la seconda viene eliminato definitivamente
          array_push($deleted, $relativefilepath);
        }
      }
    }

    return $deleted;
  }

  public static function get_sandbox() {
    $sandbox = get_option(Ro_Shared_Folders_Admin::$baseDir);
    if (empty($sandbox)) {
        $sandbox = __DIR__.'/sharedfolder';
    }
    return $sandbox;
  }

}

