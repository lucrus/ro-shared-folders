<?php
require_once(__DIR__  . '/class-ROSFSync.php');

class ROSFile_MetaKeys {
    public static $fileid = "rosf-autopost-fileid";

    public static $type_slug = 'rosfile_type';

    public static $case_study_filter_slug = 'rosf_cstud_filter';
}

class ROSFile {
    public static $visibleNameFieldPrefix = "rosf_visible_name_";

    function __construct() {
        add_action( 'init', array($this, 'register_cpt'));
        add_action( 'init', array($this, 'register_tax'));
        add_action( "template_redirect", array($this, "redirect"));
    }
  
    // let's create the function for the custom type
    public function register_cpt() { 
        // creating (registering) the custom type 
        register_post_type( ROSFile_MetaKeys::$type_slug, /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
            // let's now add all the options for this post type
            array( 'labels' => array(
                'name' => __( 'ROS files', 'ro-shared-folders' ), /* This is the Title of the Group */
                'singular_name' => __( 'ROS file', 'ro-shared-folders' ), /* This is the individual type */
                'all_items' => __( 'All files', 'ro-shared-folders' ), /* the all items menu item */
                'add_new' => __( 'Add file', 'ro-shared-folders' ), /* The add new menu item */
                'add_new_item' => __( 'Add a new file', 'ro-shared-folders' ), /* Add New Display Title */
                'edit' => __( 'Edit', 'ro-shared-folders' ), /* Edit Dialog */
                'edit_item' => __( 'Edit file', 'ro-shared-folders' ), /* Edit Display Title */
                'new_item' => __( 'New file', 'ro-shared-folders' ), /* New Display Title */
                'view_item' => __( 'Show file', 'ro-shared-folders' ), /* View Display Title */
                'search_items' => __( 'Search files', 'ro-shared-folders' ), /* Search Custom Type Title */ 
                'not_found' =>  __( 'No files found', 'ro-shared-folders' ), /* This displays if there are no entries yet */ 
                'not_found_in_trash' => __( 'No files in trash', 'ro-shared-folders' ), /* This displays if there is nothing in the trash */
                'parent_item_colon' => ''
                ), /* end of labels */
                'description' => __( 'ROS Files', 'ro-shared-folders' ), /* Custom Type Description */
                'public' => false,
                'publicly_queryable' => false,
                'exclude_from_search' => true,
                'show_ui' => true,
                'query_var' => true,
                'menu_position' => 82, /* this is what order you want it to appear in on the left hand side menu */ 
                'menu_icon' => 'dashicons-list-view', /* the icon for the custom post type menu */
                'rewrite'	=> array( 'slug' => ROSFile_MetaKeys::$type_slug, 'with_front' => true ), /* you can specify its url slug */
                'has_archive' => false, /* you can rename the slug here */
                'hierarchical' => false,
                'show_in_rest' => false,
                'show_in_menu' => true,
                /* the next one is important, it tells what's enabled in the post editor */
                'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'sticky', 'page-attributes')
            ) /* end of options */
        ); /* end of register post type */
	} 

    public static function getTaxonomySlugFromName($name) {
        return substr($name, 0, 32);
    }

    public function register_tax() {
        $tnames = Ro_Shared_Folders_Admin::getTaxonomyNames();
        $taxonomies = [];
        $ords = ["1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "very deep"];
        foreach ($tnames as $idx => $name) {
            $slug = self::getTaxonomySlugFromName($name);
            $ordinal = $ords[min(sizeof($ords) - 1, $idx)];
            $tax = register_taxonomy($slug, ROSFile_MetaKeys::$type_slug, 
            array( 'labels' => array(
                'name' => __( $name, 'ro-shared-folders' ), 
                'singular_name' => __( $name, 'ro-shared-folders' ), 
                'all_items' => __( 'All '.$name, 'ro-shared-folders' ), 
                'add_new' => __( 'Add '.$name, 'ro-shared-folders' ), 
                'add_new_item' => __( 'Add a new '.$name, 'ro-shared-folders' ), 
                'edit' => __( 'Edit', 'ro-shared-folders' ), 
                'edit_item' => __( 'Edit '.$name, 'ro-shared-folders' ), 
                'new_item' => __( 'New '.$name, 'ro-shared-folders' ), 
                'view_item' => __( 'Show '.$name, 'ro-shared-folders' ), 
                'search_items' => __( 'Search '.$name, 'ro-shared-folders' ),
                'not_found' =>  __( 'No '.$name.' found', 'ro-shared-folders' ),
                'not_found_in_trash' => __( 'No '.$name.' in trash', 'ro-shared-folders' ),
                'parent_item_colon' => ''
                ), 
                'description' => __( $ordinal.' level ROSF directory', 'ro-shared-folders' ),
                'public' => false,
                'publicly_queryable' => true,
                'show_ui' => get_option(Ro_Shared_Folders_Admin::$debugmode),
                'hierarchical' => false,
                'show_in_nav_menus' => true,
                'show_admin_column' => true,
                'show_in_rest' => true
            ) /* end of options */);
            add_action( $slug.'_add_form_fields', ['ROSFile', 'addVisibleName'] );
            add_action( $slug.'_edit_form_fields', ['ROSFile', 'editVisibleName'], 10, 2 );
            add_action( 'created_'.$slug, ['ROSFile', 'saveVisibleName'] );
            add_action( 'edited_'.$slug, ['ROSFile', 'saveVisibleName'] );
            add_filter( 'manage_edit-'.$slug.'_columns', ['ROSFile', 'showVisiblenameColumn'] );
            add_action( 'manage_'.$slug.'_custom_column', ['ROSFile', 'fillVisiblenameColumn'], 10, 3 );

            array_push($taxonomies, $tax);
        } 
        $tax = register_taxonomy(ROSFile_MetaKeys::$case_study_filter_slug, 'post', 
        array( 'labels' => array(
            'name' => __( "Case studies filters", 'ro-shared-folders' ), 
            'singular_name' => __( 'Filter', 'ro-shared-folders' ), 
            'all_items' => __( 'All filters', 'ro-shared-folders' ), 
            'add_new' => __( 'Add filter', 'ro-shared-folders' ), 
            'add_new_item' => __( 'Add a new filter', 'ro-shared-folders' ), 
            'edit' => __( 'Edit', 'ro-shared-folders' ), 
            'edit_item' => __( 'Edit filter', 'ro-shared-folders' ), 
            'new_item' => __( 'New filter', 'ro-shared-folders' ), 
            'view_item' => __( 'Show filter', 'ro-shared-folders' ), 
            'search_items' => __( 'Search filter', 'ro-shared-folders' ),
            'not_found' =>  __( 'No filters found', 'ro-shared-folders' ),
            'not_found_in_trash' => __( 'No filters in trash', 'ro-shared-folders' ),
            'parent_item_colon' => ''
            ), 
            'description' => __('A tag used as a filter for the case studies shortcodes', 'ro-shared-folders' ),
            'public' => false,
            'publicly_queryable' => true,
            'show_ui' => true,
            'hierarchical' => true,
            'show_in_nav_menus' => true,
            'show_admin_column' => true,
            'show_in_rest' => true
        ) /* end of options */);

        array_push($taxonomies, $tax);
        return $taxonomies;   
    }

    public static function addVisibleName( $taxonomy ) {
        $id = self::$visibleNameFieldPrefix.$taxonomy;
        ?>
            <div class="form-field">
                <label for="<?php echo $id ?>">Visible Name</label>
                <input type="text" name="<?php echo $id ?>" id="<?php echo $id ?>" />
                <p>Term visible name</p>
            </div>
        <?php
    }

    public static function editVisibleName($term, $taxonomy) {
        $id = self::$visibleNameFieldPrefix.$taxonomy;
	    // get meta data value
	    $text_field = get_term_meta( $term->term_id, $id, true );

	?><tr class="form-field">
        <th>
            <label for="<?php echo $id ?>">Visible Name</label>
        </th>
		<td>
            <input type="text" name="<?php echo $id ?>" id="<?php echo $id ?>" value="<?php echo esc_attr( $text_field ) ?>"/>
            <p class="description">Term visible name</p>
		</td>
	</tr>
<?php        
    }

    public static function saveVisibleName( $term_id ) {
        $term = get_term($term_id);
        $metaname = self::$visibleNameFieldPrefix.$term->taxonomy;
        update_term_meta(
            $term_id,
            $metaname,
            filter_input(INPUT_POST, $metaname)
        );
    }

    public static function showVisiblenameColumn( $columns ) {
        $columns['visiblename'] = __( 'Visible name' );
        return $columns;
    }            

    public static function fillVisiblenameColumn( $string, $columns, $term_id ) {
        $term = get_term($term_id);
        $metaname = self::$visibleNameFieldPrefix.$term->taxonomy;
        switch ( $columns ) {
            case 'visiblename' :
                echo esc_html( get_term_meta( $term_id, $metaname, true ) );
            break;
        }
    }

    public function redirect() {
        global $post;
        if (is_singular(ROSFile_MetaKeys::$type_slug)) {
            $redir = null;
            $crp_links = get_post_meta($post->ID, "crp_relations_to", false); // We support "Custom Related Posts" plugin
            if (count($crp_links) > 0) {
                $redir = $crp_links[0][array_keys($crp_links[0])[0]]["permalink"];
            } else if (has_excerpt($post)) {
                $redir = wp_strip_all_tags(get_the_excerpt($post));
            } 
            if (empty($redir)) {
                status_header(410); // gone
                header("X-Robots-Tag: noindex");
            } else {
                status_header(302); // temporary redirect
                header("Location: ".$redir);
            }
            exit;
        }        
    }
}

?>
