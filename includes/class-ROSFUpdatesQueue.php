<?php

require_once(__DIR__.'/ROSFJsCallbackManager.php');
require_once(__DIR__.'/../admin/class-ro-shared-folders-admin.php');
require_once(__DIR__.'/class-ROSF_DBFifo.php');


class ROSFUpdateStep {
  public $payload;
  public $updaterFunction;
}

class ROSFUpdatesQueue {

  public static string $wpcronHookName = 'rosf_queue_update';
  public static string $wpcronInterval = 'rosf_queue_update_interval';

  private static $fifo = null;

  public function __construct() {
    
    // self::initCronHook();
    add_action( 'rest_api_init', array($this, 'registerRoutes'), 999, 0); 

  }
  
  public function registerRoutes()
  {
    register_rest_route(ROSFJsCallbackManager::$namespace, '/getUpdateProgress', array(
    'methods'  => WP_REST_Server::READABLE,
    'callback' => array('ROSFUpdatesQueue', 'getProgress'),
    'permission_callback' => function () {
      return true;
    }));
  }

  /**
   * if the function is called from Ajax, then we have a non null request
   * and it returns an array ["stepstogo" => number ]. Otherwise, if
   * it is called from PHP, it just returns the number
   */ 
  public static function getProgress(WP_REST_Request $request = null) {
    if (!empty($request)) {
      $exec = $request->get_param("exec");
      if (!empty($exec)) {
        if ($exec === "true") {
          self::next(true);
        }
      }
    }
    $result = self::$fifo->size();
    return (empty($request)) ? $result : ["stepstogo" => $result];
  }

  private static function initFifo() {
    if (self::$fifo === null) {
      self::$fifo = new ROSF_DBFifo();
    }
  }

  public static function add($steps) {
    self::initFifo();
    self::$fifo->push($steps);
  }

  public static function batchExec($queue) {

    $index = 0;
    while ($index < count($queue)) {
      
      $step = $queue[$index];
      try {
        call_user_func($step->updaterFunction, $step->payload);
      } catch (Exception $e) {
        error_log($e, 0);
      }

      $index++;
    }    
  }

  public static function next($fromAjaxOrCLI = false) {
    self::initFifo();

    $elapsed = 0;
    $start = hrtime(true);

    $queue = self::$fifo->peek();
    if (empty($queue) || !is_array($queue)) {
      return false;
    }

    $maxmillis = get_option(Ro_Shared_Folders_Admin::$maxMillisecondsPerStep, 100);
    if (!is_numeric($maxmillis)) {
      $maxmillis = intval($maxmillis);
    }
    if ($fromAjaxOrCLI) {
      $maxmillis = PHP_INT_MAX;
    }

    $index = 0;
    while ($elapsed < $maxmillis && $index < count($queue)) {
      
      $step = $queue[$index];
      try {
        call_user_func($step->updaterFunction, $step->payload);
      } catch (Exception $e) {
        error_log($e, 0);
      }

      $now = hrtime(true);
      $elapsed = ($now - $start) / 1e+6; // convert nanoseconds to milliseconds
      $index++;
    }
    
    self::$fifo->remove($index);
  }

  public static function initCronHook() {
    /*
    add_action(self::$wpcronHookName, function () { self::next(); });
    
    add_filter('cron_schedules', function ($schedules) {
      $schedules[ROSFUpdatesQueue::$wpcronInterval] = array(
        'interval' => 1,
        'display' => __( 'Every second' )
      );
      return $schedules;
    }); 
    */
  }
}