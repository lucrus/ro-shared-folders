<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/includes
 * @author     Virtual Bit <info@virtualbit.it>
 */
class Ro_Shared_Folders {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Ro_Shared_Folders_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */

	/**
	 * The base directory of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $basedir    The current version of the plugin.
	 */
	private $basedir;

	public static $directdownloadpageslug = "rosf_directdownloadpage";

	public static Ro_Shared_Folders $instance;

	public ROSFJsCallbackManager $jsCallbackManager;
	public ROSFListFiles $listFiles;
	public ROSFSelectMenu $selectMenu;
	public ROSFile $cpt;
	public Ro_Shared_Folders_Admin $plugin_admin;
	public ROSFSync $sync;


	public function __construct($rosf_basedir) {
		if (empty(self::$instance)) {
			self::$instance = $this;
		}
		if ( defined( 'RO_SHARED_FOLDERS_VERSION' ) ) {
			$this->version = RO_SHARED_FOLDERS_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'ro-shared-folders';
		$this->basedir = $rosf_basedir;

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

		$this->registerCliCommands();

	}

	public function registerCliCommands() {
		if ( class_exists( 'WP_CLI' ) ) {
			WP_CLI::add_command( 'rosf sync', [$this->sync, 'sync'] );
		}		
	}


	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Ro_Shared_Folders_Loader. Orchestrates the hooks of the plugin.
	 * - Ro_Shared_Folders_i18n. Defines internationalization functionality.
	 * - Ro_Shared_Folders_Admin. Defines all hooks for the admin area.
	 * - Ro_Shared_Folders_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		$bd = $this->basedir;

		require_once $bd.'includes/class-ro-shared-folders-loader.php';
		require_once $bd.'includes/class-ro-shared-folders-i18n.php';
		require_once $bd.'admin/class-ro-shared-folders-admin.php';
		require_once $bd.'public/class-ro-shared-folders-public.php';

		$this->loader = new Ro_Shared_Folders_Loader();

		require_once $bd.'includes/class-ROSFShortcodes.php';
		require_once $bd.'includes/class-ROSFListFiles.php';
		require_once $bd.'includes/class-ROSFSelectMenu.php';
		require_once $bd.'includes/class-ROSFile.php';
		require_once $bd.'includes/ROSFJsCallbackManager.php';
		require_once $bd.'includes/class-ROSFUpdatesQueue.php';
		require_once $bd.'includes/class-ROSFSync.php';

		$this->jsCallbackManager = new ROSFJsCallbackManager('public'); // any handle will do
		$this->listFiles = new ROSFListFiles();
		$this->selectMenu = new ROSFSelectMenu();
		$this->cpt = new ROSFile();
		$this->sync = new ROSFSync();
		
		// la classe activator schedula la next() di ROSFUpdatesQueue, attraverso wp-cron,
		// ogni secondo. Tuttavia non possiamo affidarci esclusivamente a wp-cron, perché
		// non sappiamo se sia configurato correttamente a livello di server. Aggiungiamo
		// quindi una chiamata alla next ad ogni caircamento pagina, che male non fa ( se
		// possa far male in realtà dipende dal numero di millis di esecuzione configurati
		// dall'admin nell'apposita casella della pagina di configurazione di questo plg).

		// Tutto molto bello, ma fa casino, quindi l'ho disabilitato e convertito in comando wp-cli
		// add_action('init', array('ROSFUpdatesQueue', 'next'));
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Ro_Shared_Folders_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Ro_Shared_Folders_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$this->plugin_admin = new Ro_Shared_Folders_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $this->plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $this->plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action('admin_init', $this->plugin_admin, 'register_settings');
		$this->loader->add_action('admin_menu', $this->plugin_admin, 'menu');

		add_filter('login_redirect', array($this, 'redirect_to_direct_download'), 10, 3);
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
		
		$plugin_public = new Ro_Shared_Folders_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));

		add_filter( 'page_template', array($this, 'direct_downloads' ));
	}

	public function redirect_to_direct_download($redirect_to, $request, $user) {
		if (isset($user->roles) && is_array($user->roles) ) {
			$startedHere = false;
			if (session_status() === PHP_SESSION_NONE) {
				session_start();
				$startedHere = true;
			}
			if (isset($_SESSION["rosf_redirect_to"])) {
				$redirect_to = $_SESSION["rosf_redirect_to"];
				unset($_SESSION["rosf_redirect_to"]);
			}
			if ($startedHere) {
				session_destroy();
			}
		}
		return $redirect_to;
	}


	public function direct_downloads( $page_template )
	{
	  if (is_page(self::$directdownloadpageslug)) 
	  {
		$page_template = $this->basedir . 'includes/direct-downloads.php';
	  }
	  return $page_template;
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();

		$scclasses = array('ROSFShortcodes' => 'rosf_');
		foreach ($scclasses as $cname => $prefix)
		{
		  $class = new ReflectionClass($cname);
		  $methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
		  foreach ($methods as $m)
		  {
			$shortcodename = $prefix . $m->name;
			add_shortcode($shortcodename, array($m->class, $m->name));
		  }
		}
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Ro_Shared_Folders_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}


	public function enqueue_scripts()
	{
	  global $wp_scripts; 
	  $this->jsCallbackManager->enqueueWPApi("rosf-node-wpapi");
	  	  
	  $jsfileurl = plugin_dir_url(__FILE__) . '../public/js/ro-shared-folders-public.js';
	  wp_enqueue_script( 'jquery-ui-widget' );
	  //wp_enqueue_script('jquery-ui-selectmenu');
	  wp_enqueue_style("jquery-ui-css", "http://ajax.googleapis.com/ajax/libs/jqueryui/".$wp_scripts->registered['jquery-ui-core']->ver."/themes/ui-lightness/jquery-ui.min.css");
	  wp_enqueue_style('select2-css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
	  /*
	  wp_enqueue_style('bootstrap5-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');
	  wp_enqueue_style('s2b5-130-css', 'https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css');
	  wp_enqueue_script('bootstrap5-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js');
	  */
	  wp_enqueue_script('select2-js', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js');
	  wp_register_script($this->jsCallbackManager->getScriptHandle(), $jsfileurl, array('jquery', 'jquery-ui-widget', 'select2-js'));
  
	  $this->jsCallbackManager->callRegisteredCallbacks();
	  
	}
  
}
