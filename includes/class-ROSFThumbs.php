<?php
require_once(__DIR__.'/class-ROSF_FSCommon.php');
class ROSF_Thumbnails {

  public static function shouldRegenerateThumbnail($sandbox, $thumbsdir, $dir, $file) {
    $src = $sandbox.$dir.'/'.$file;
    $tmb = ROSF_FSCommon::thumbname($thumbsdir, $dir, $file);
    return ROSF_FSCommon::isFileNewerThan($src, $tmb);
  }
  
  public static function getB64Thumbnail($sandbox, $dir, $file) {
    $thumbsdir = get_option(Ro_Shared_Folders_Admin::$thumbsDir);
    if (!empty($thumbsdir)) {
      if (self::shouldRegenerateThumbnail($sandbox, $thumbsdir, $dir, $file)) {
        try {
          self::createOrUpdateThumbnail($sandbox, $thumbsdir, $dir, $file);
        } catch (Throwable $t) {
          error_log('Unable to make a thumbnail for "' . $dir.'/'.$file. '" in '.$thumbsdir.'. DETAILS: '.$t->getMessage());
          return false;
        }
      }
      return self::loadThumbnail($thumbsdir, $dir, $file); 
    }
    return false;
  }

  private static function loadThumbnail($thumbsdir, $dir, $file) {
    $fname = ROSF_FSCommon::thumbname($thumbsdir, $dir, $file);
    try {
      if (file_exists($fname)) {
        $filesize = filesize($fname);
        $fp = fopen($fname, 'rb');
        if ($fp === false) {
          return false;
        }
        try {
          return base64_encode(fread($fp, $filesize));
        } finally {
          fclose($fp);
        }
      } else 
        return false;
    } catch (Throwable $e) {
      error_log('Unable to load the thumbnail for "' . $dir.'/'.$file. '" in '.$thumbsdir.'. DETAILS: '.$e->getMessage());
      return false;
    }
  }

  function imagecreatefromjpegexif($filename)
  {
      $img = imagecreatefromjpeg($filename);
      $exif = exif_read_data($filename);
      if ($img && $exif && isset($exif['Orientation']))
      {
          $ort = $exif['Orientation'];

          if ($ort == 6 || $ort == 5)
              $img = imagerotate($img, 270, 0xFFFFFFFF);
          if ($ort == 3 || $ort == 4)
              $img = imagerotate($img, 180, 0xFFFFFFFF);
          if ($ort == 8 || $ort == 7)
              $img = imagerotate($img, 90, 0xFFFFFFFF);

          if ($ort == 5 || $ort == 4 || $ort == 7)
              imageflip($img, IMG_FLIP_HORIZONTAL);
      }
      return $img;
  }

  private function imageCreateFromAny($filepath) {
    $type = exif_imagetype($filepath); 
    $im = false;
    switch ($type) {
        case IMAGETYPE_GIF :
            $im = imageCreateFromGif($filepath);
            break;
        case IMAGETYPE_JPEG :
            $im = self::imagecreatefromjpegexif($filepath);
            break;
        case IMAGETYPE_PNG :
            $im = imageCreateFromPng($filepath);
            break;
    }   
    return $im; 
  }

  private static function createOrUpdateThumbnail($sandbox, $thumbsdir, $dir, $file)
  {
    $im = null;
    $filename = $sandbox.$dir.'/'.$file;
    if (str_ends_with(strtolower($filename), '.pdf')) {
      if (class_exists('Imagick')) {
        try {
          $im = new Imagick($filename.'[0]');
          if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('#ffffff');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            $im = $im->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
          }
          $im->setImageFormat('jpg');  
          $im->setResolution(20,20);
          ROSF_FSCommon::mkdir($thumbsdir.$dir);
          $im->writeImage(ROSF_FSCommon::thumbname($thumbsdir, $dir, $file));
        } catch (Throwable $t) {
          error_log("Could not generate thumbnail for PDF file: ". $filename . " DETAILS: " . $t->getMessage());
        }
      }
    } else {
      $arr_image_details = getimagesize($filename);
      $original_width = $arr_image_details[0];
      $original_height = $arr_image_details[1];
      if ($original_width > $original_height) {
          $new_width = 256;
          $new_height = intval($original_height * $new_width / $original_width);
      } else {
          $new_height = 256;
          $new_width = intval($original_width * $new_height / $original_height);
      }
      $im = self::imageCreateFromAny($filename);
      if ($im) {
        $new_image = imagecreatetruecolor($new_width, $new_height);
        imagecopyresized($new_image, $im, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);
        ROSF_FSCommon::mkdir($thumbsdir.$dir);
        imagejpeg($new_image, ROSF_FSCommon::thumbname($thumbsdir, $dir, $file));
      }
    }
  }
}
