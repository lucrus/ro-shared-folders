<?php

class ROSFCaseStudies {

  private static $filterParam = 'rosf-cs-filter';
  private static $limitFilterParam = 'rosf-cs-limitfilter';
  private static $topOfPageId = 'rosf-cs-topofpage';

  private $resultsCount = 0;
  
  public function resultsHtml(string $cat, int $strt = 0, int $numb = 5 ) {
    $cats = [];
    if (strlen($cat) == 0) {
      $limitcat = filter_input(INPUT_GET, self::$limitFilterParam);
      if (strlen($limitcat) > 0)
        array_push($cats, $limitcat);
      $fcat = filter_input(INPUT_GET, self::$filterParam);
      if (strlen($fcat) > 0)
        array_push($cats, $fcat);
    }
    $posts = $this->get_posts_by_taxonomy_query($cats, $strt, $numb);
    $result = '<div class="rosf-cs-page">';
    foreach ($posts as $p) {
      $result .= '<a class="rosf-cs-link" href="'.get_post_permalink($p).'">';
        $result .= '<div class="rosf-cs-post">'; 

          $result .= '<div class="rosf-cs-title">';
            $result .= '<span class="rosf-cs-title-text">'.get_the_title($p).'</span>';
          $result .= '</div>'; 

          $result .= '<div class="rosf-cs-excerpt">'; 
            $result .= $this->generate_excerpt($p, 100);
          $result .= '</div>'; 
          
          $result .= '<div class="rosf-cs-readmore">'; 
            $result .= '<span class="rosf-cs-readmore-text">Leggi tutto</span>';
          $result .= '</div>'; 

          $result .= '<div class="rosf-cs-image">'; 
            $result .= '<div class="rosf-cs-image-bg" style="background-image: url('."'".get_the_post_thumbnail_url($p, 'full')."'".')"></div>';
          $result .= '</div>'; 

        $result .= '</div>'; 
      $result .= '</a>'; 
    }
    $result .= '</div>';
    if ($strt + $numb < $this->resultsCount - 1) {
      $result .= '<div class="rosf-cs-loadmore" data-strt="'.($strt + $numb).'" data-numb="'.$numb.'">Carica altri</div>"';
    }
    return $result;
  }

  public function topOfPageHtml() {
    return '<span id="'.self::$topOfPageId.'"></span>';
  }

  public function filtersHtml() {
    $preselct = filter_input(INPUT_GET, self::$filterParam);
    $onlythis = filter_input(INPUT_GET, self::$limitFilterParam);
    $terms = $this->get_filters();
    $filters = '';
    $idsuffix = bin2hex(random_bytes(4));
    $formid = 'rosf-cs-filters-form-'.$idsuffix;
    $count = 0;
    $tuttiChecked = ' checked';
    foreach ( $terms as $term ) {
      if ($onlythis != null) { 
        // se l'URL mi sta forzando un particolare filtro, devo capire se il $term 
        // attuale sia quel filtro, o un suo figlio, o non abbia nulla a che vedere.
        // Se è figlio, lo mostro, ma negli altri due casi evito di includerlo: nel
        // caso in cui sia esattamente il filtro forzato, è inutile mostrarlo, mentre,
        // nel caso in cui sia un filtro estraneo, ovviamente non devo mostrarlo.
        if (strcmp($term->slug, $onlythis) == 0) { // esattamente quello forzato, per cui salto.
          continue; 
        } else { // estraneo o figlio, devo capire quale dei due.
          $parent = $term;
          $found = false;
          while ($parent->parent && !$found) {
            $parent = get_term($parent->parent, ROSFile_MetaKeys::$case_study_filter_slug);
            $found = $found || (strcmp($parent->slug, $onlythis) == 0);
          }
          if (!$found) continue;
        }
      } else {
        // l'URL non sta forzando un particolare filtro, quindi devo mostrare solo
        // i $term di primo livello (senza parent)
        if ($term->parent) continue;
      }

      $checked = '';
      if (strcmp($term->slug, $preselct) == 0) {
        $checked = ' checked';
        $tuttiChecked = '';
      }
      $id = $formid.'_'.$term->slug;
      $filters .= '<div class="rosf-sc-filter">'; 
        $filters .= '<input type="radio" id="'.$id.'" name="'.self::$filterParam.'" value="'.$term->slug.'"'.$checked.' onClick="document.body.style.cursor='."'".'wait'."'".';document.getElementById('."'".$formid."'".').submit();"/>';
        $filters .= '<label class="rosf-sc-filter-label" for="'.$id.'">'.$term->name.'</label>';
      $filters .= '</div>';
      $count++;
    }
    
    // se alla fine di tutto ciò nei filtri c'è una sola voce e l'URL
    // non mi sta forzando alcun filtro, allora è inutile mostrarla, perché i case
    // studies che vengono mostrati sono solo  gli articoli che hanno almeno
    // una categoria impostata fra quelle della tassonomia "case studies 
    // filters", quindi se qui abbiamo una sola voce, quella selezionerebbe
    // comunque tutti gli articoli già mostrati.
    // Se invece l'URL mi sta forzando un particolare filtro, allora qui nei filtri
    // potrbbe esserci una sola voce, ma non è detto che tutti gli articoli mostrati
    // abbiano quella categoria, perché questa voce potrebbe essere un filtro
    // figlio di quello forzato ed alcuni articoli potrebbero avere solo la 
    // categoria del parent forzato, ma non quella del figlio qui incluso nei filtri:
    // in questo caso la mostro anche se è una sola, aggiungendo poi il "mostra tutti".
    // Se non abbiamo alcuna voce, significa che l'URL ci sta forzando un filtro e non
    // ci sono categorie figlie di quello forzato: evitiamo di mostrare il box
    // vuoto contenente nessun filtro.
    $minvoci = $onlythis == null ? 1 : 0; 
    if ($count <= $minvoci) return '';

    $tuttiftl = '<div class="rosf-sc-filter">'; 
      $tuttiftl .= '<input type="radio" id="'.$formid.'-all" name="'.self::$filterParam.'" value=""'.$tuttiChecked.' onClick="document.body.style.cursor='."'".'wait'."'".';document.getElementById('."'".$formid."'".').submit();"/>';
      $tuttiftl .= '<label class="rosf-sc-filter-label" for="'.$formid.'-all">'
                        .get_option(Ro_Shared_Folders_Admin::$csfiltersAllLabel, 'View all')
                    .'</label>';
    $tuttiftl .= '</div>';

    $filters = $tuttiftl.$filters;

    $result = '<form id="'.$formid.'" method="get" action="#'.self::$topOfPageId.'"><fieldset class="rosf-sc-filters">';
    $result .= '<legend class="rosf-sc-filters-title">'.get_option(Ro_Shared_Folders_Admin::$csfiltersTitle, 'Filters').'</legend>';
    $result .= $filters;
    if ($onlythis) {
      $result .= '<input type="hidden" name="'.self::$limitFilterParam.'" value="'.$onlythis.'">';
    }
    $result .= '</fieldset></form>'; 
    return $result;
  }

  private function get_filters() {
    return get_terms( array(
      'taxonomy'   => ROSFile_MetaKeys::$case_study_filter_slug,
      'hide_empty' => true,
      ) );
  }

  public function get_posts_by_taxonomy_query(Array $cats = [], int $strt = 0, int $numb = 5 ) {
    $tax_query = [];
    $relation = "AND"; // di default, se mi specificano più di un filtro, li metto in AND

    // se però non è stata selezionata alcuna categoria dei "case studies", cioè l'array è vuoto, 
    // allora prendo tutti i post che hanno una qualsiasi fra le categorie esistenti della tassonomia
    // "case study filters" e devo quindi metterle in "OR"

    if (count($cats) == 0) {
      $cats = array_map(fn($term): string => $term->slug, $this->get_filters());
      $relation = "OR";
    }
    foreach ($cats as $c) {
      $tq_filter = [
        'taxonomy' => ROSFile_MetaKeys::$case_study_filter_slug,
        'field'    => 'slug',
        'terms'    => [sanitize_title($c)]
      ];
      array_push($tax_query, $tq_filter);
    }
    if (count($tax_query) > 1) {
      $tax_query["relation"] = $relation;
    }
    $args = [
       'posts_per_page'         => $numb,
       'post_type'              => 'post',
       'no_found_rows'          => true,
       'update_post_meta_cache' => false,
       'update_post_term_cache' => false,
       'tax_query'              => $tax_query,
       'offset'                 => $strt 
    ];

    $post_tax_query  = new WP_Query($args);
    return $post_tax_query->posts;
  }

  public function strip_shortcodes($content) {
    $opensquare = mb_strpos($content, '[');
    if ($opensquare === false) {
      return $content;
    }
    $closesquare = mb_strpos($content, ']', $opensquare);
    if ($closesquare === false) {
      return $content;
    }
    $shortc = mb_substr($content, $opensquare, $closesquare - $opensquare);
    $nestedoffset = 0;
    while (($nested = mb_strpos($shortc, '[', $nestedoffset + 1)) !== false)
      $nestedoffset = $nested;
    $opensquare += $nestedoffset;

    return $this->strip_shortcodes(mb_substr($content, 0, $opensquare).mb_substr($content, $closesquare + 1));
  }

  public function generate_excerpt($post, int $limit) {
    $content = get_the_excerpt($post);
    if (!$content || mb_strlen($content) == 0) 
      $content = get_the_content(null, false, $post);
    $content = strip_tags($content);
    $content = $this->strip_shortcodes($content);
    $content = trim(preg_replace('/\s+/', ' ', $content));
    $ret = $content; 
    $ellipsis = '';
    if (mb_strlen($content) >= $limit) {
      $ellipsis = '…';
       $ret = mb_substr($content, 0, $limit);
       if (mb_substr($ret, -1) !== ' ') {
          $space_pos_in_substr = mb_strrpos($ret, ' ');
          $space_pos_in_content = mb_strpos($content, ' ', $limit);
          if ($space_pos_in_content != false && $space_pos_in_content - $limit <= $limit - $space_pos_in_substr) {
             $ret = mb_substr($content, 0, $space_pos_in_content);
          } else {
             $ret = mb_substr($content, 0, $space_pos_in_substr);
          }
       }
    }
    return $ret . $ellipsis;
  }
}