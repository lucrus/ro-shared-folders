<?php

require_once(__DIR__ . '/ROSFJsCallbackManager.php');
require_once(__DIR__ . '/../admin/class-ro-shared-folders-admin.php');


class ROSF_DBFifo
{
  private array $currentInBlock;
  private array $currentOutBlock;
  private array $superblock;

  public function __construct()
  {
    define('ROSF_DBFIFO_ITEMS_PER_BLOCK', 20000); // NEVER CHANGE THESE, OR YOU WILL LOSE DATA ON UPGRADES
    define('ROSF_DBFIFO_MAX_ITEMS', 1000000000); // NEVER CHANGE THESE, OR YOU WILL LOSE DATA ON UPGRADES
    define('ROSF_DBFIFO_INDEX_FORMAT', '%09d'); // NEVER CHANGE THESE, OR YOU WILL LOSE DATA ON UPGRADES
    define('ROSF_DBFIFO_SBINDEX_IN', 0); // NEVER CHANGE THESE, OR YOU WILL LOSE DATA ON UPGRADES
    define('ROSF_DBFIFO_SBINDEX_OUT', 1); // NEVER CHANGE THESE, OR YOU WILL LOSE DATA ON UPGRADES
    define('ROSF_DBFIFO_SBINDEX_COUNT', 2); // NEVER CHANGE THESE, OR YOU WILL LOSE DATA ON UPGRADES

    $this->currentInBlock = [];
    $this->currentOutBlock = [];
    $this->superblock = [];
  }

  private function saveSuperblock()
  {
    update_option(Ro_Shared_Folders_Admin::$blockOptsSuperblock, $this->superblock, true);
  }

  private function getValueFromSuperblock(int $sb_index)
  {
    if (count($this->superblock) === 0) {
      $this->superblock = get_option(Ro_Shared_Folders_Admin::$blockOptsSuperblock, []);
      if (count($this->superblock) === 0) {
        $this->superblock = [0, 0, 0];
        $this->saveSuperblock();
      }
    }
    return $this->superblock[$sb_index];
  }

  private function getInIndexFromSuperblock()
  {
    return $this->getValueFromSuperblock(ROSF_DBFIFO_SBINDEX_IN);
  }

  private function getOutIndexFromSuperblock()
  {
    return $this->getValueFromSuperblock(ROSF_DBFIFO_SBINDEX_OUT);
  }

  private function getItemsCountFromSuperblock()
  {
    return $this->getValueFromSuperblock(ROSF_DBFIFO_SBINDEX_COUNT);
  }

  private function saveValueIntoSuperblock($sb_index, $index)
  {
    $this->superblock[$sb_index] = $index;
  }

  private function saveInIndexIntoSuperblock($index)
  {
    $this->saveValueIntoSuperblock(ROSF_DBFIFO_SBINDEX_IN, $index);
  }

  private function saveOutIndexIntoSuperblock($index)
  {
    $this->saveValueIntoSuperblock(ROSF_DBFIFO_SBINDEX_OUT, $index);
  }

  private function saveCountIntoSuperblock($count)
  {
    $this->saveValueIntoSuperblock(ROSF_DBFIFO_SBINDEX_COUNT, $count);
  }

  private function getBlockNumberFromIndex(int $realIndex)
  {
    return intval(floor($realIndex / ROSF_DBFIFO_ITEMS_PER_BLOCK));
  }

  private function getOptionName(int $index)
  {
    $blockNum = $this->getBlockNumberFromIndex($index);
    $result = sprintf(Ro_Shared_Folders_Admin::$blockOptsPrefix . ROSF_DBFIFO_INDEX_FORMAT, $blockNum);
    return $result;
  }

  private function loadBlock(int $index)
  {
    $option = $this->getOptionName($index);
    $data = get_option($option, false);
    if (false === $data) {
      $data = [];
    }
    return $data;
  }

  private function saveBlock(array $data)
  {
    $option = $this->getOptionName($this->getInIndexFromSuperblock());
    update_option($option, $data, false);
  }

  private function dropOutBlock()
  {
    $option = $this->getOptionName($this->getOutIndexFromSuperblock());
    delete_option($option);
    $this->currentOutBlock = [];
  }

  private function saveInBlock()
  {
    $this->saveBlock($this->currentInBlock);
  }

  private function saveInBlockAndSuperblock($inIndex, $count) {
    $this->saveInBlock();

    $this->saveInIndexIntoSuperblock($inIndex);
    $this->saveCountIntoSuperblock($count);
    $this->saveSuperblock();
  }

  public function push(array $objects)
  {
    $semkey = ftok(".", ".");
    $sem = sem_get($semkey);
    $inIndex = $this->getInIndexFromSuperblock();
    $count = $this->getItemsCountFromSuperblock();
    if (($count + count($objects)) > ROSF_DBFIFO_MAX_ITEMS)
      return false;

    $blockIndex = ROSF_DBFIFO_ITEMS_PER_BLOCK;
    for ($i = 0; $i < count($objects); $i++) {
      sem_acquire($sem);
      try {
        if ($blockIndex >= ROSF_DBFIFO_ITEMS_PER_BLOCK) {
          if ($i > 0) {
            $this->saveInBlockAndSuperblock($inIndex, $count);
          }
          $this->currentInBlock = $this->loadBlock($inIndex);
          $blockIndex = $inIndex % ROSF_DBFIFO_ITEMS_PER_BLOCK;
        }
        $this->currentInBlock[$blockIndex] = $objects[$i];
        $blockIndex++;
        $inIndex = (($inIndex + 1) % ROSF_DBFIFO_MAX_ITEMS);
        $count++;
      } finally {
        sem_release($sem);
      }
    }
    $this->saveInBlockAndSuperblock($inIndex, $count);
    return true;
  }

  public function peek()
  {
    $semkey = ftok(".", ".");
    $sem = sem_get($semkey);
    sem_acquire($sem);
    try {
      $count = $this->getItemsCountFromSuperblock();
      if ($count === 0) {
        return [];
      }
      $outIndex = $this->getOutIndexFromSuperblock();
      $this->currentOutBlock = $this->loadBlock($outIndex);
      $blockIndex = $outIndex % ROSF_DBFIFO_ITEMS_PER_BLOCK;
      $result = array_slice($this->currentOutBlock, $blockIndex);
      return $result;
    } finally {
      sem_release($sem);
    }
  }

  public function remove(int $howmany, array $donequeue = null)
  {
    $semkey = ftok(".", ".");
    $sem = sem_get($semkey);
    sem_acquire($sem);
    try {
      $count = $this->getItemsCountFromSuperblock();
      $outIndex = $this->getOutIndexFromSuperblock();
      if ((($outIndex + $howmany) % ROSF_DBFIFO_ITEMS_PER_BLOCK) === 0) {
        $this->dropOutBlock();
      }
      $outIndex += $howmany;
      $count -= $howmany;
      $this->saveOutIndexIntoSuperblock($outIndex);
      $this->saveCountIntoSuperblock($count);
      $this->saveSuperblock();
    } finally {
      sem_release($sem);
    }
  }

  public function size()
  {
    return $this->getItemsCountFromSuperblock();
  }
}
