<?php
/**
 * Description of JsCallbackManager
 */
class ROSFJsCallbackManager
{

  private $script_handle;
  private $target;

  public static $namespace = "rosf/v1";

  /**
   * @var $script_params_callbacks []
   */
  private $script_params_callbacks;


  public function __construct($target)
  {
    $this->target = $target;
    $this->script_handle = 'rosf_ajax_'.$target;
    $this->script_params_callbacks = array();
    $this->registerScriptParamsCallback(array($this, "setupDefaults"));
    $this->registerScriptParamsCallback(array($this, "redirectIfNoAuth"));
  }
  
  /**
   * This function calls the registered callbacks in the same order they were
   * registered. See registerScriptParamsCallback in this class.
   * This function is intended to be executed during the wp_enqueue_scripts hook.
   * This function calls each callback, it provides it with the registered
   * params and it expects a array as a result of calling it.
   * The resulting array must contain a JS event name (such as 'load', 'click'
   * and so on) and other parameters that the $(document).ready JS function will
   * receive, parse and execute accordingly. 
   * See ro-shared-folders-public.js for details.
   * rofshooks_params is the vector that makes the communication possible: it
   * is the resulting array of arrays: actually it is just a pair whose key is
   * always 'events' and whose value is always the array of the callbacks results.
   * @param array $hooks_data_events
   */
  public function callRegisteredCallbacks()
  {
    $hooks_data_events = array();

    foreach ($this->script_params_callbacks as $callback_pack)
    {
      $callback = $callback_pack['call'];
      $params = $callback_pack['params'];
      $cbarr = $callback($params);
      array_unshift($cbarr, $this->target);
      $hooks_data_events[count($hooks_data_events)] = $cbarr;
    }

    $hooks_data = array('events' => $hooks_data_events);

    wp_localize_script($this->script_handle, "rofshooks_params", $hooks_data);
    wp_enqueue_script($this->script_handle);
  }

  /**
   * This function saves the callable so that the wp_enqueue_scripts event will
   * call it afterwards (see callRegisteredCallbacks function in this class). 
   * This registerScriptParamsCallback function is intened to be used in 
   * constructors or other functions that are executed before said event takes
   * place and that need to hook that event, but in a guaranteed call order.
   * The callRegisteredCallbacks  function guaratees that the registered 
   * callbacks will be called in the same order they were registered (FIFO order).
   * Maybe WP code that handles hooks does just the same, but, since, even if it
   * did, that's not documented behavior, we cannot rely upon what it 
   * incidentally does as of today.
   * @param Callable $callable_arg The callback
   * @param array $params The params the callback will receive
   * @return boolean is_callable($callable_arg)
   */
  public function registerScriptParamsCallback($callable_arg, array $params = array())
  {
    if (is_callable($callable_arg))
      array_push($this->script_params_callbacks, array('call' => $callable_arg, 'params' => $params));
    return is_callable($callable_arg);
  }
  
  public function getScriptHandle()
  {
    return $this->script_handle;
  }
  
  public function enqueueWPApi()
  {
    $wpapifileurl = plugin_dir_url(__FILE__) . '../public/js/wpapi/wpapi.min.js';
    $wpapihandle = "wpv-node-wpapi";
    wp_register_script($wpapihandle, $wpapifileurl);
    wp_enqueue_script($wpapihandle);    
  }

  public function setupDefaults(array $params)
  {  
    return array('load', 
                 'setupDefaults', 
                  array(get_rest_url(),
                        self::$namespace,
                        wp_create_nonce('wp_rest')
                        ));    
  }

  public function redirectIfNoAuth(array $params)
  { 
    global $wp;
    $redirect = !(current_user_can(ROSFSync::$rosf_base_cap) || ROSFPermissions::isAdminUser());
    $args = [ROSFPermissions::$login_autoredirect_id];
    if ($redirect) {
      $redirtag = "rosfrediractivated";
      $alreadyset = count($wp->query_vars);
      foreach ($wp->query_vars as $k => $v) {
        if (stripos($k, $redirtag) === false && stripos($v, $redirtag) === false) {
          $alreadyset--;
        }
      }
      if ($alreadyset === 0) {
        $redirect_back = urlencode(add_query_arg( $wp->query_vars, home_url($wp->request))); 
        array_push($args, '/login/?redirect_uri='.$redirect_back."&redirect_to=".$redirect_back.'&'.$redirtag.'='.$redirtag);
      }
    }
    return array('load', 
                 'redirectTo', 
                  $args); 
  }
}
