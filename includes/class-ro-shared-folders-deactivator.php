<?php

require_once(__DIR__."/class-ROSFUpdatesQueue.php");

/**
 * Fired during plugin deactivation
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/includes
 * @author     Virtual Bit <info@virtualbit.it>
 */
class Ro_Shared_Folders_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		/*
		ROSFUpdatesQueue::initCronHook();

		$timestamp = wp_next_scheduled(ROSFUpdatesQueue::$wpcronHookName);
		wp_unschedule_event( $timestamp, ROSFUpdatesQueue::$wpcronHookName);
		*/
	}

}
