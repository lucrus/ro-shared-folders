<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/includes
 * @author     Virtual Bit <info@virtualbit.it>
 */
class Ro_Shared_Folders_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ro-shared-folders',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
