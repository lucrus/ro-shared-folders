<?php

require_once(__DIR__."/class-ROSFTreeView.php");
require_once(__DIR__."/class-ROSFSelectMenu.php");
require_once(__DIR__."/class-ROSFNewsletterView.php");
require_once(__DIR__."/class-ROSFSync.php");
require_once(__DIR__."/class-ROSFCaseStudies.php");

/**
 * Description of ROSFShortcodes
 */
 class ROSFShortcodes {

  private static $conditions = [];

  public static function dirview($atts, $content = '')
  {
    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);    

    // first of all, we sandbox the directory to show to avoid
    // security issues such as showing /etc/shadow and the like
    $sandbox = ROSFSync::get_sandbox();

    $basedir = ROSFSync::sanitize_path($basedir);

    // please note that it is still possible to escape the sandbox
    // if the sysadmin made links inside it, or bind-mounted something
    // inside it, or something like that, but we can't avoid those
    // things here, and we probably don't want to.

    return ROSFTreeView::html($sandbox, $basedir);
  }

  public static function fileslist($atts, $content = '')
  {
    $skip_wrapper = 'false';

    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  
    
    $result = '<div id="rosf-fileslist"/>';
    
    if ('false' === $skip_wrapper) {
      $result = '<div id="'.ROSFListFiles::$target.'">'.$result."</div>";
    }
    
    return $result;
  }

  public static function selectmenu($atts, $content = '') {

    $depth = 0;

    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  

    if ($depth !== 0) {
      $depth = intval($depth);
    }


    $result = Ro_Shared_Folders::$instance->selectMenu->html($depth, ROSFSelectMenu::$target);
    
    return $result;
  }

  public static function filenamefilter($atts, $content = '') {


    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  

    $result = Ro_Shared_Folders::$instance->selectMenu->htmlFilenameFilter(ROSFSelectMenu::$target);
    
    return $result;
  }

  public static function resetfilters($atts, $content = '') {

    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  
    
    return '<button id="rosf-resetfilters">X</button>';
  }

  public static function searchresults($atts, $content = '') {
    
    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE); 

    $result = '<div class="'.ROSFSelectMenu::$target.'-wrapper">';
      $result .= '<div class="rosf-selectmenu-results-panel '.ROSFSelectMenu::$target.'">';
      $result .= '</div>';
      $result .= '<div class="rosf-loadmore" data-target="'.ROSFSelectMenu::$target.'">Carica altri...</div>';
    $result .= '</div>';
    return $result;
  }
  
    
  public static function newest($atts, $content = '') {
    $limit = 3;

    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  

    if ($limit !== 3) {
      $limit = intval($limit);
    }

    $result = Ro_Shared_Folders::$instance->listFiles->htmlNewest($limit);

    $result = '<div id="rosf-newest-wrapper">'.$result."</div>";
    
    return $result;
  }

  public static function loadingmask($atts, $content = '') {
    return '<div id="rosf-loading" style="background-image: url('."'".ROSFListFiles::themeFile('loading.gif')."'".');"></div>';
  }

  public static function newsletter($atts, $content = '') {

    $showcode = false;

    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  

    return ROSFNewsletterView::single($showcode, Ro_Shared_Folders::$instance->listFiles);
  }

  public static function newsletters($atts, $content = '') {

    return ROSFNewsletterView::table(Ro_Shared_Folders::$instance->listFiles);
  }

  public static function debug($atts, $content = '') {
    $function = '';

    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  

    if (!empty($function)) {
      if (ROSFPermissions::isAdminUser()) {
        eval($function);
      }
    }
  }

  public static function login_autoredirect($atts, $content = '') {
    return '<div id="'.ROSFPermissions::$login_autoredirect_id.'" style="display: none !important"></div>';
  }

  public static function condition($atts, $content = '') {

    $logged_in = "true";
    $display = "normal";

    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  
    
    $logged_in = ($logged_in === "true");

    if (($logged_in && is_user_logged_in()) || 
      (!$logged_in && !is_user_logged_in())) {
        $open_div = !($display === "normal"); 
        if ($content === '') {
          array_push(self::$conditions, $open_div);
          return $open_div ? '<div style="display: none !important;">' : '';
        } else {
          return $open_div ? '' : $content;
        }
      }
  }

  public static function close_condition($atts, $content = '') {

    $to_close = array_pop(self::$conditions);
    if ($to_close) {
        return '</div>';
    }
  }

  public static function cs_posts($atts, $content = '')
  {
    $cat = '';
    $strt = '0';
    $numb = '10';

    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  
    
    $pos = new ROSFCaseStudies();    
    return $pos->resultsHtml($cat, intval($strt), intval($numb));
  }
  
  public static function cs_topofpage($atts, $content = '') {
    $pos = new ROSFCaseStudies();    
    return $pos->topOfPageHtml();
  }


  public static function cs_filters($atts, $content = '')
  {
    if (is_array($atts))
      extract($atts, EXTR_OVERWRITE);  
    $pos = new ROSFCaseStudies();    
    return $pos->filtersHtml();
  }

}