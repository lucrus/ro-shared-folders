<?php

require_once(__DIR__.'/class-ROSF_FSCommon.php');

class ROSFTreeView extends ROSF_FSCommon {


  public static function hotHtml(ROSF_DirectoryDescriptor $dir, $dirzipmarkup = "") {
    $result = '';
    $hotclass = '';
    if ($dir->hot) {
      $hotclass = ' rosf-hot rosf-hotdir';
    }

    $result .= '<div class="rosf-directory-name'.$hotclass.'" data-rosf-id="'.$dir->curdir.'" data-rosf-contained-in="'.$dir->curdir.'" data-rosf-depth="'.$dir->depth.'">';
      $result .= '<span class="rosf-closed-directory-icon rosf-directory-icon" data-rosf-id="'.$dir->curdir.'">┕</span>';
      $result .= '<span class="rosf-open-directory-icon rosf-directory-icon" data-rosf-id="'.$dir->curdir.'">⬊</span>';
      $hotflag = '<span class="rosf-hotflag" style="background-image: url(';
      $hotflag.= "'".ROSFListFiles::themeFile('hot.png')."'";
      $hotflag.= ');"></span>';
      $result .= '<span class="rosf-directory-name-text'.$hotclass.'">'.$dir->subdir.'</span>'.$dirzipmarkup.$hotflag;
      $permalink = self::getDirectoryPermalink($dir);
      $result .= '<span class="rosf-permalink" data-permalink="'.$permalink.'"></span>';
    $result .= '</div>';
    return $result;
  }

  private static function recursiveHtml(Array $dirlist, string $cd) {
    $result = '';
    foreach ($dirlist as $dir) {
      $classes="rosf-directory";
      if ($cd === $dir->curdir) {
        $classes .= ' rosf-permalinked';
      }
      $result .= '<div class="'.$classes.'" data-rosf-id="'.$dir->curdir.'" data-rosf-depth="'.$dir->depth.'">';

      $result .= self::hotHtml($dir);

      if ($dir->subtree) {
        $result .= self::recursiveHtml($dir->subtree, $cd);
      }

      $result .= '</div>';
    }
    return $result;
  }

  public static function html($sandbox, $basedir) {
    $dirs = self::_listOnlyDirectories($sandbox, $basedir);

    $dir = self::sharingKeyToPathComponent(filter_input(INPUT_GET, self::$sharingKeyDirParam));
    $result = self::recursiveHtml($dirs, $dir);
    $result .= self::getSharingHtml();
    return $result;
  }


  public static function singleDirHtml(ROSF_DirectoryDescriptor $dir) {

    $markup = '<div>';
    $markup .= '<a href="' . self::getDirectoryPermalink($dir) . '">'.$dir->subdir.'</a>';
    $markup .= ' ('.$dir->parent->curdir.')';
    $markup .= '</div>';

    return $markup;
  }
  
}