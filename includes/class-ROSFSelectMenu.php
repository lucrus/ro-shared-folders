<?php

require_once(__DIR__.'/class-ROSF_FSCommon.php');

// function remove_template_endpoints( $endpoints ) {
//  foreach ( $endpoints as $endpoint => $details ) {
//      if ( fnmatch( '/wp/v2/template*/*', $endpoint ) ) {
//          unset( $endpoints[$endpoint] );
//      }
//  }
//
//  return $endpoints;
// }
// add_filter( 'rest_endpoints', 'remove_template_endpoints' );

class ROSFSelectMenu extends ROSF_FSCommon {

  public static $wrapper = 'rosf-selectmenu-wrapper';
  public static $target = 'rosf-selectmenu-results';
  public static $maxResultsPerPage = 6;


  /**
   * @var      string    $filterparam    Nome del parametro che contiene tutte le selezioni operate dall'utente nelle <select> per la ricerca.
   *                                     È utilizzato solo via REST API. Contiene un array dove l'elemento 0 corrisponde
   *                                     alle opzioni scelte nella <select> con depth = 0, l'elemento 1 quelle della 
   *                                     <select> con depth = 1, ecc...
   *                                     Ogni elemento è a sua volta rappresentato con un array, che contiene ogni opzione
   *                                     selezionata. Un'opzione selezionata è una stringa, corrispondente all'attributo "value" della
   *                                     rispettiva <option>. 
   *                                     Il tutto codificato JSON.
   *                                     Esempio:
   * 
   *                                     [["option3"],["option1","option4"]]
   * 
   *                                     indica che nella <select> con depth=0 è stata scelta la option3 e nella
   *                                     <select> con depth=1 sono state scelte la prima e la quarta.
   */
  public static $filterparam = "rosf_flt";
  public static $filenamefilter = "rosf_fnflt";
  public static $page = "rosf_page";

  public function __construct() {
  
    add_action( 'rest_api_init', array($this, 'registerRoutes'), 999, 0); 
  }

  public function registerRoutes()
  {
    register_rest_route(ROSFJsCallbackManager::$namespace, '/search', array(
    'methods'  => WP_REST_Server::READABLE,
    'callback' => array($this, 'search'),
    'permission_callback' => function () {
      return current_user_can(ROSFSync::$rosf_base_cap) || self::isAdminUser();
    }));
  }

  public function htmlFilenameFilter($target) {
    $result = '<div class="rosf-select-wrapper">';
    $result .= '<label for="rosf-filenamefilter" class="rosf-select-title rofs-select-title-filenamefilter">'.
                  '<span class="rosf-select-title-text">Nome del file</span><br>';
    $result .= '<input type="text" name="rosf-filenamefilter" id="rosf-filenamefilter" data-target="'.$target.'"></label></div>';
    return $result;
  }
  
  public function html($depth, $target) {

    $taxnames = Ro_Shared_Folders_Admin::getTaxonomyNames();
    if ($depth > count($taxnames) - 1) {
      $depth = count($taxnames) - 1;
    }

    $tax = $taxnames[$depth];

    $attributes = array (
			'taxonomy' => $tax,
			'orderby' => 'name',
			'order' => 'ASC',
			'hide_empty' => true
	  );

    $terms = get_terms($attributes);

    $options = "";
    $termscount = 0;
    foreach ($terms as $term) {
      $dirs = explode("||", html_entity_decode($term->description));
      foreach ($dirs as $dir) {
        if (ROSFSync::currentUserCanAccess($dir)) {
          $options .= '<option value="' . $term->slug .'" data-curdir="'.$dir.'">' . ucfirst($term->name) .'</option>';
          $termscount++;
          break;
        }
      }
    }

    $result = '<div class="rosf-select-wrapper">';
    $result .= '<label for="rosf-select-'.$depth.'" class="rosf-select-title rofs-select-title-'.$depth.'">'.
                  '<span class="rosf-select-title-text">'.$taxnames[$depth].'</span><br>';

    $hiddenselectclass = '';
    if ($termscount <= 1) {
      $hiddenselectclass = ' rosf-hidden-select';
    }

    $result .= '<select name="rosf-select-'.$depth.'" id="rosf-select-'.$depth.'"'.
                'class="rosf-menudepth'.$depth.' rosf-select'.$hiddenselectclass.'" '.
                'data-depth="'.$depth.'" '.
                'data-target="'.$target.'">';
    
    if ($termscount > 1) {
      $result .= '<option value="*" selected>Qualsiasi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>';
    } else {
      $options = '<option selected '.substr($options, strlen('<option '));
    }

    $result .= $options;

    $result .= '</select></label>';
    $result .= '</div>';
    $hideSelect = '';

    if ($termscount <= 1) {
      $hideSelect = ' style="display: none" ';
    }

    $result = '<div id="'.self::$wrapper.$depth.'" '.$hideSelect.'>'.$result."</div>";
    return $result;
  }

  public function get_posts_by_taxonomy_query(Array $taxonomies, Array $term_names, string $keyword = null, int $page = 0 ) {
    $tax_query = [];
    for ($i = 0; $i < count($taxonomies); $i++) {
      if ($term_names[$i] == "*")
        continue;
      $tq_filter = [
        'taxonomy' => $taxonomies[$i],
        'field'    => 'slug',
        'terms'    => [sanitize_title($term_names[$i])],   // TODO: multiselect support
      ];
      array_push($tax_query, $tq_filter);
    }
    if (count($tax_query) > 1) {
      $tax_query["relation"] = "AND";
    }
    $args = [
       'posts_per_page'         => self::$maxResultsPerPage,
       'post_type'              => 'rosfile_type',
       'no_found_rows'          => true,
       'update_post_meta_cache' => false,
       'update_post_term_cache' => false,
       'tax_query'              => $tax_query,
       'paged'                  => $page
    ];

    if (!empty($keyword)) {
      $args['s'] = $keyword;
    }
 
    $post_tax_query  = new WP_Query($args);
    return $post_tax_query->posts; 
  }

  private function convertPostsToFilesDescriptors(Array $posts, $sandbox) {
    $fds = [];
    for ($i = 0; $i < count($posts); $i++) {
      $post = $posts[$i];
      $fullrelativepath = '/'.get_post_meta($post->ID, "rosf_file_id", true);
      $parent = dirname($fullrelativepath);
      if (ROSFSync::currentUserCanAccess($parent)) {
        $fd = new ROSF_FileDescriptor();
        $fd->name = basename($fullrelativepath);
        $fd->parentDir = $parent;
        $fd->hot = self::_isNew($sandbox, $fd->parentDir, $fd, self::_getNWYTTime());
        array_push($fds, $fd);
      }
    }

    usort($fds, function ($a, $b) {
      return $b->mtime - $a->mtime;
    });
    
    return $fds;
  }

  private function resultsHtml(Array $posts) {
    $result = '<div class="rosf-results" style="display: none">';
    $sandbox = ROSFSync::get_sandbox();

    $fds = $this->convertPostsToFilesDescriptors($posts, $sandbox);

    $result .= '<ul class="rosf-fileslist rosf-fileslist-noborders">';
    
    foreach ($fds as $fd) {
      $result .= ROSFListFiles::singleFileHtml($sandbox, $fd->parentDir, $fd->parentDir, $fd->name);
    }

    $result .= '</ul>';

    if (count($fds) == 0) {
      $result .= '<div class="rosf-no-results">Nessun risultato per questa ricerca: forse potresti rimuovere uno dei filtri.</div>';
    }

    $result .= '</div>';
    return $result;
  }

  private function allStars(Array $filters) {
    $result = true;
    for ($i = 0; $i < count($filters) && $result; $i++) {
      if (is_array($filters[$i])) {
        $result = $result && $this->allStars($filters[$i]);
      } else
        $result = $result && ($filters[$i] == '*');
    }
    return $result;
  }

  public function search(WP_REST_Request $request) {
    $fnfilter = $request->get_param(self::$filenamefilter);
    $sfilters = $request->get_param(self::$filterparam);
    $afilters = explode(',', $sfilters);
    $page = $request->get_param(self::$page);
    $posts = Array();

    if ($this->allStars($afilters) && empty($fnfilter)) {
      $markup = '<div class="rosf-no-filters" style="display: none" >Per eseguire la ricerca è necessario impostare almeno un filtro.</div>';
    } else {
      $taxo = explode(',', get_option(Ro_Shared_Folders_Admin::$subdirTaxonomies, []));

      $posts = $this->get_posts_by_taxonomy_query($taxo, $afilters, $fnfilter, (intval($page) + 1));
      $markup = $this->resultsHtml($posts);
    }

    $result = ["markup" => $markup,
               "loadmore" => (sizeof($posts) == self::$maxResultsPerPage)];
    
    return $result;
  }

}