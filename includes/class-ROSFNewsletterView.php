<?php

require_once(__DIR__."/class-ROSFTreeView.php");
require_once(__DIR__."/class-ROSFSelectMenu.php");

/**
 * Description of ROSFShortcodes
 */
 class ROSFNewsletterView extends ROSF_FSCommon {

  public static function single(bool $showcode, ROSFListFiles $filesLister) {

    $sandbox = ROSFSync::get_sandbox();
    $newest = $filesLister->findHottestDirs($sandbox);
    $result = '<ul class="rosf-dirlist">';
    foreach ($newest as $new) {
      $result .= '<li>';
      $result .= ROSFTreeView::singleDirHtml($new);  
      $newfiles = ROSFListFiles::_listOnlyFiles($sandbox, $new->curdir);
      $result .= "<ul>";
      foreach ($newfiles as $nf) {
        if (!$nf->hot)
          continue;
          $result .= '<li>';
          $result .= '<a href="'.ROSF_FSCommon::getFilePermalink($nf).'">'.$nf->name.'</a>';
          $result .= '</li>';
        }
      $result .= "</ul>";
      $result .= '</li>';
    }
    $result .= '</ul>';

    if ($showcode) {
      $result = htmlspecialchars($result);
    }

    return $result;
  }


  public static function table(ROSFListFiles $filesLister) {

    if (!ROSFSync::isAdminUser()) {
      return '';
    }
    
    $result = '<table class="newsletters-table">';
    $result .= '<thead>';    
      $result .= '<tr>';    
        $result .= '<th>';    
          $result .= 'Accessi';    
        $result .= '</th>';    
        $result .= '<th>';    
          $result .= 'Anteprima newsletter';    
        $result .= '</th>';    
        $result .= '<th>';    
          $result .= 'Codice HTML newsletter';    
        $result .= '</th>';    
        $result .= '<th>';    
          $result .= 'Elenco utenti abilitati';    
        $result .= '</th>';    
      $result .= '</tr>';    
    $result .= '</thead>';    
    $result .= '<tbody>';    
 
    $taxnames = Ro_Shared_Folders_Admin::getTaxonomyNames();
    $tax = $taxnames[0];

    $clause = array (
			'taxonomy' => $tax,
			'orderby' => 'name',
			'order' => 'ASC',
			'hide_empty' => true
	  );

    $terms = get_terms($clause);
    $capabilities = [];
    foreach ($terms as $t) {
      $dirs = explode("||", $t->description);
      foreach ($dirs as $dir) {
        $capabilities[] = ROSFSync::getDirectoryCapabilityName(htmlspecialchars_decode($dir));
        $capabilities[] = ROSFSync::getDirectoryDeepCapabilityName(htmlspecialchars_decode($dir));
      }
    }

    $prot = Ro_Shared_Folders_Admin::getProtectedFolders();
    foreach ($prot as $pf) {
      $capabilities[] = ROSFPermissions::getDirectoryCapabilityName($pf);
      $capabilities[] = ROSFPermissions::getDirectoryDeepCapabilityName($pf);
    }

    $combinazioniCaps = ROSFPermissions::calcolaTutteLeCombinazioni($capabilities);
    $users = get_users();

    foreach ($combinazioniCaps as $cc) {     

      $autorizzati = ROSFPermissions::getUtentiConCaps($cc, $users);
      if (count($autorizzati) == 0) continue;

      $users = array_udiff($users, $autorizzati, function (WP_User $a, WP_User $b) {
        return $a->ID - $b->ID;
      });

      ROSFPermissions::$simulatedCapabilities = $cc;
      $result .= '<tr>';
        $result .= '<td>';
          $result .= '<ul>';
            $viewableCaps = self::collapseCaps($cc);
            foreach ($viewableCaps as $sc) {
              $result .= '<li>'.$sc.'</li>';
            }
          $result .= '</ul>';
        $result .= '</td>';

        $result .= '<td>';
          $result .= self::single(false, $filesLister); // preview
        $result .= '</td>';
        $result .= '<td>';
          $result .= '<textarea readonly class="rosf-newsletterhtmlcode">';
            $result .= self::single(true, $filesLister); // html snippet
          $result .= '</textarea>';
        $result .= '</td>';
        $result .= '<td>';
          $result .= '<ul>';
          foreach ($autorizzati as $u) {
            $result .= '<li>'.$u->user_email.'</li>';
          }
          $result .= '</ul>';
        $result .= '</td>';
      $result .= '</tr>';
      ROSFPermissions::$simulatedCapabilities = [];
    } 
    $result .= '</tbody>';    
    $result .= '</table>';    
    return $result;
  }

  private static function collapseCaps(array $caps) {
    array_walk($caps, function (&$cap) {
       
      $plain = self::getPlainCapabilityNameFromDeep($cap);
      $prefix = self::$rosf_base_cap."_";
      if (strpos($plain, $prefix) === 0) {
        $plain = substr($plain, strlen($prefix));
      }
      $cap = $plain;
    });

    $caps = array_filter(array_unique($caps));
    return $caps;
  }
}