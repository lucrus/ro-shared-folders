<?php

require_once(__DIR__.'/class-ROSF_FSCommon.php');

if (! function_exists('str_ends_with')) {
  function str_ends_with(string $haystack, string $needle): bool
  {
      $needle_len = strlen($needle);
      return ($needle_len === 0 || 0 === substr_compare($haystack, $needle, - $needle_len));
  }
}

class ROSFListFiles extends ROSF_FSCommon {

  public static $target = 'rosf-fileslist-wrapper';

  private static $rrcolor;

  public function __construct() {
    self::$rrcolor = 0;
    add_action( 'rest_api_init', array($this, 'registerRoutes'), 999, 0); 
  }

  public function registerRoutes()
  {
    register_rest_route(ROSFJsCallbackManager::$namespace, '/listFiles', array(
    'methods'  => WP_REST_Server::READABLE,
    'callback' => array($this, 'listFiles'),
    'permission_callback' => function () {
      return current_user_can(ROSFSync::$rosf_base_cap) || self::isAdminUser();
    }));
    register_rest_route(ROSFJsCallbackManager::$namespace, '/download', array(
      'methods'  => WP_REST_Server::READABLE,
      'callback' => array($this, 'download'),
      'permission_callback' => function () {
        return current_user_can(ROSFSync::$rosf_base_cap) || self::isAdminUser();
    }));
    register_rest_route(ROSFJsCallbackManager::$namespace, '/zipdown', array(
      'methods'  => WP_REST_Server::READABLE,
      'callback' => array($this, 'zipdown'),
      'permission_callback' => function () {
        return current_user_can(ROSFSync::$rosf_base_cap) || self::isAdminUser();
    }));
  }

  public static function singleFileHtml($sandbox, $dir, $dirid, $filename, $isNew = null, $dirarg = null) {
    if (null === $dirarg) {
      $dirarg = $dirid;
    }

    if (null === $isNew) {
      $isNew = self::_isNew($sandbox, $dir, $filename, self::_getNWYTTime());
    }

    $markup = '';
    $encfile = self::getFileNamePermalink($dirarg, $filename);
    $encdir = self::getDirectoryNamePermalink($dirarg);
    $fileurl = wp_nonce_url(get_rest_url().ROSFJsCallbackManager::$namespace.
                                        '/download?dir='.$encdir.
                                        '&file='.$encfile, 'wp_rest');
    $markup .= '<li data-rosf-dir-id="'.$dirid.'" data-rosf-file-id="'.$filename.'" class="rosf-downloadable-file">';
    if ($isNew) {
      $hotstyle = 'style="background-image: url('."'".self::themeFile('hotribbon.png')."'".');"';
      $markup .= '<div class="rosf-file-ribbon" '.$hotstyle.'></div>';
    }
    $markup .= '<a href="'.$fileurl.'">';
    $colorclass = 'altcolor'.self::$rrcolor;
    self::$rrcolor = (self::$rrcolor + 1) % 8;
    $markup .= '<div class="rosf-file-wrapper '.$colorclass.'">';

      $markup .= '<div class="rosf-file-icon-wrapper">';

        $iconurl = self::iconUrl($sandbox, $dir, $filename);
        $iconclasses = ['rosf-file-icon'];
        if (strpos($iconurl, "data:image") === 0) {
          array_push($iconclasses, "rosf-real-preview");
        }
        $markup .= '<div class="'.implode(' ', $iconclasses).'" style="background-image: url('."'".$iconurl."'".');">';
        $markup .= '</div>';
      
      $markup .= '</div>';

      $markup .= '<div class="rosf-file-name" title="'.$filename.'">';
      $markup .= $filename;
      $markup .= '</div>';
    
    $markup .= '</div>';
    $markup .= '</a>';
    $markup .= self::singleFileTags($dir, $filename, $isNew, $fileurl);
    $markup .= '</li>';
    
    return $markup;
  }

  public static function singleFileTags($dir, $filename, $isNew, $fileurl) {
    $posts = ROSFSync::getPostsByPathAndFileName($dir.'/'.$filename)->posts;
    $taxo = array_map(function ($tax) { return ROSFile::getTaxonomySlugFromName($tax); }, 
                      Ro_Shared_Folders_Admin::getTaxonomyNames());
    $terms = [];
    foreach ($taxo as $tax) {
      $terms = array_merge($terms, wp_get_post_terms($posts[0]->ID, $tax));
    }
    $tnames = array_map(function ($term) {
      return get_term_meta($term->term_id, ROSFile::$visibleNameFieldPrefix.$term->taxonomy, true);
    }, $terms);
    $result = '<div class="rosf-file-tags">';
    
    $tags = '#'.implode(' &nbsp; &nbsp; #', $tnames);     
    /*
    $sdir = trim($dir, '/');
    $tags = '#'.str_replace('/', ' &nbsp; &nbsp; #', $sdir);
    */
    
    $result .= '<div class="rosf-file-tag">'.$tags.'</div>';
    $result .= '</div>';

    return $result;
  }

  public function htmlNewest($limit) {
    $sandbox = ROSFSync::get_sandbox();
    $newest = self::findHottestFiles($sandbox, $limit);
    $result = '<div class="rosf-fileslist rosf-fileslist-noborders">';
    foreach ($newest as $new) {
      $result .= self::singleFileHtml($sandbox, $new->parentDir, $new->parentDir, $new->name, $new->hot);  
    }
    $result .= '</div>';
    return $result;
  }

  public function listFiles(WP_REST_Request $request) {
    $dirid = $request->get_param("rosfId");
    $sandbox = ROSFSync::get_sandbox();
    $dir = ROSFSync::sanitize_path($dirid);

    if (!empty($dir) && strpos($dir, '/') !== 0) {
      $dir = '/'.$dir;
    }

    $files = self::_listOnlyFiles($sandbox, $dir);
    
    $markup = '<div class="rosf-fileslist-box">';    
      $markup .= '<ul class="rosf-fileslist">';
      
      foreach ($files as $fd) {
        $markup .= self::singleFileHtml($sandbox, $dir, $dirid, $fd->name, $fd->hot, $dirid);
        $markup .= '<span class="rosf-permalink" data-permalink="'.self::getFilePermalink($fd).'"></span>';
      }

      $markup .= '</ul>'; 
    $markup .= self::zipHtml($dir, sizeof($files));

    if (count($files) === 0) {
      $markup .= '<p><span class="rosf-no-files-message">'.
                  __('La cartella', 'ro-shared-folders').
                  ' <span class="rosf-empty-folder-name">'.$dir.'</span> '.
                  __('non contiene alcun file.', 'ro-shared-folders').
                  '</span></p>'.
                  '<p><span class="rosf-hope-message">'.
                  __('Potresti comunque trovare dei files in eventuali sue sottocartelle.', 'ro-shared-folders')
                  .'</span></p>';
    }

    $markup .= '</div>'; 
    
    $result = ["target" => "#".self::$target,
               "markup" => $markup];

    return $result;
  }

  public static function iconUrl($sandbox, $dir, $filename) {
    $fullPathAndName = $sandbox.$dir.'/'.$filename;
    $mime_type = mime_content_type($fullPathAndName);
    $url = false;
    switch ($mime_type) {
      case "application/pdf": 
      case "image/png":
      case "image/jpeg":
      case "image/gif":
      case "image/tiff":
      case "image/webp":
      case "image/svg":
        $url = ROSF_Thumbnails::getB64Thumbnail($sandbox, $dir, $filename);
        if ($url !== false) {
          $url = 'data:image/jpeg;base64,'.$url;
        }
        break;
    }
    if ($url === false) {
      $fext = $filename;
      $liod = strrpos($filename, '.');
      if ($liod !== false) {
        $liod++;
        $fext = substr($filename, $liod);
      }

      $iconfilesext = ['*.webp', '*.png', '*.jpg', '*.jpeg', '*.gif'];
      $themeFolder = get_option(Ro_Shared_Folders_Admin::$themeFolder, __DIR__.'/../images/');
      if (!str_ends_with($themeFolder, '/')) {
        $themeFolder .= '/';
      }
      $mimedir = $themeFolder . 'mime/' . $mime_type;
      $iconfile = false;
      foreach ($iconfilesext as $ife) {
        $ifiles = $mimedir . '/' . $ife;
        $flist = glob($ifiles);
        if (count($flist) > 0) {
          $iconfile = $flist[0];
          break;
        }
      }

      if (empty($iconfile)) {
        $fextdir = $themeFolder . 'filesext/' . $fext;
        foreach ($iconfilesext as $ife) {
          $ifiles = $fextdir . '/' . $ife;
          $flist = glob($ifiles);
          if (count($flist) > 0) {
            $iconfile = $flist[0];
            break;
          }
        }
      }

      if (empty($iconfile)) {
        $iconfile = $themeFolder.'placeholder.webp?mime='.urlencode($mime_type).'&ext='.urlencode($fext);
      }

      $url = self::pathToUrl($iconfile);
    }
    return $url;
  }

  public static function pathToUrl( $path ) {
    // Get correct URL and path to wp-content
    $content_url = untrailingslashit( dirname( dirname( get_stylesheet_directory_uri() ) ) );
    $content_dir = untrailingslashit( WP_CONTENT_DIR );
    
    return str_replace( $content_dir, $content_url, $path);
  }

  public function download(WP_REST_Request $request) {
    $coded_dir = $request->get_param(self::$sharingKeyDirParam);
    $dir = self::sanitize_path(self::sharingKeyToPathComponent($coded_dir));

    if (!empty($dir) && strpos($dir, '/') !== 0) {
      $dir = '/'.$dir;
    }

    if (ROSFSync::currentUserCanAccess($dir)) {
      $sandbox = ROSFSync::get_sandbox();
      $coded_file = $request->get_param(self::$sharingKeyFileParam);
      $file = self::sanitize_path(self::sharingKeyToPathComponent($coded_file));

      if (!$file) {
        $thumbsdir = get_option(Ro_Shared_Folders_Admin::$thumbsDir);
        self::zipdown($sandbox, $thumbsdir, $dir);
      } else {
        self::filedown($sandbox, $dir, $file);
      }
    }
    exit;
  }

  private static function filedown(string $sandbox, string $dir, string $file) {
    $file = ROSFSync::sanitize_path($file);

    $path = $sandbox.$dir.'/'.$file;
    $public_name = basename($path);
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime_type = finfo_file($finfo, $path);

    header("Content-Disposition: attachment; filename=$public_name;");
    header("Content-Type: $mime_type");
    header("Content-Description: File Transfer");
    header("Content-Transfer-Encoding: binary");
    header('Content-Length: ' . filesize($path));
    header("Cache-Control: no-cache private");

    $fp = fopen($path, 'rb');
    ob_end_flush();
    fpassthru($fp);
  }

  private static function zipdown(string $sandbox, string $thumbsdir, string $dir) {
    $destdir = dirname($thumbsdir.$dir);
    $lastdircomponent = basename($dir);
    $destzip = $destdir.'/'.$lastdircomponent.'.zip';
    $srcdir = $sandbox.$dir;
    if (self::shouldRegenerateZip($srcdir, $destzip)) {
      $file_names = self::_listOnlyFiles($sandbox, $dir);    
      $zip = new ZipArchive();  
      self::mkdir($destdir);
      touch($destzip);
      $zip->open($destzip, ZipArchive::OVERWRITE);
      foreach($file_names as $file) {
        $zip->addFile($sandbox.$dir.'/'.$file->name, $file->name);
      }
      $zip->close();
    }
    $archive_file_name=substr(strtr($dir, "/ \\", "___").'.zip', 1);
    //then send the headers to force download the zip file
    header("Content-type: application/zip"); 
    header("Content-Disposition: attachment; filename=$archive_file_name"); 
    header("Cache-Control: max-age=3600"); 
    header("Content-length: " . filesize($destzip));
    header("Cache-Control: no-store"); 
    $fp = fopen($destzip, 'rb');
    ob_end_flush();
    fpassthru($fp);
  }

  public static function zipHtml(string $dir, int $nFiles) {

    $dirzipmarkup = '';
    if ($nFiles > 0) {
      $encdir = self::getDirectoryNamePermalink($dir);
      $dirurl = wp_nonce_url(get_rest_url().ROSFJsCallbackManager::$namespace.'/download?dir='.$encdir, 'wp_rest');
      $dirzipmarkup = '<a class="rosf-downloadable-dir-wrapper" href="'.$dirurl.'">';
      $dirzipmarkup .= '<span data-rosf-dirzip-id="'.$dir.
                              '" class="rosf-downloadable-dir" style="background-image: url(';

      $dirzipmarkup .= "'".self::themeFile('zipdown.png')."');".'"></span>';
      $dirzipmarkup .= '</a>';
    }
  
    return $dirzipmarkup;
  }
  
  public static function themeFile($filename, $dir = '.') {
    $themeFolder = get_option(Ro_Shared_Folders_Admin::$themeFolder, false);
    if ($themeFolder) {
      if (!str_ends_with($themeFolder, '/')) {
        $themeFolder .= '/';
      }
      $file = $themeFolder.$dir.'/'.$filename;
      if (file_exists($file)) {
        return self::pathToUrl($file);
      }
    }
    return plugin_dir_url(__FILE__).'../images/'.$dir.'/'.$filename;

  }

  public static function shouldRegenerateZip($dir, $zipfile) {
    $result = false;
    foreach (scandir($dir) as $subfile) {
      if (self::shouldSkip($subfile))
        continue;
      $curfile = $dir . "/" . $subfile;
      if (self::isFileNewerThan($curfile, $zipfile)) {
        $result = true;
        break;
      }
    }
    return $result;
  }

  public static function directDownload() {
    $coded_dir = filter_input(INPUT_GET, self::$sharingKeyDirParam);
    $coded_file = filter_input(INPUT_GET, self::$sharingKeyFileParam);
    $dir = self::sharingKeyToPathComponent($coded_dir);
    $file = self::sharingKeyToPathComponent($coded_file);
    $sandbox = ROSFSync::get_sandbox();
    if (ROSFSync::currentUserCanAccess($dir)) {
      if (!empty($file)) {
        self::filedown($sandbox, $dir, $file);
      } else {
        $thumbsdir = get_option(Ro_Shared_Folders_Admin::$thumbsDir);
        self::zipdown($sandbox, $thumbsdir, $dir);
      }
      die();
    } else {
      global $wp;
      $cur_url = add_query_arg($wp->query_vars, home_url($wp->request));
      $cur_url .= '&' . http_build_query([self::$sharingKeyDirParam => $coded_dir, self::$sharingKeyFileParam => $coded_file]);
      if (session_status() === PHP_SESSION_NONE) {
        session_start();
        $_SESSION["rosf_redirect_to"] = $cur_url;
      }
      wp_redirect(wp_login_url());
    }
  }

}