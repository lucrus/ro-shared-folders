<?php

require_once(__DIR__."/../includes/class-ROSFSync.php");

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/admin
 * @author     Virtual Bit <info@virtualbit.it>
 */
class Ro_Shared_Folders_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	
	private static $options_group;
	private static $plugin_options_slug;
	
	public static $baseDir;
	public static $thumbsDir;
	public static $newWhenYoungerThan;
	public static $maxMillisecondsPerStep;
	public static $subdirTaxonomies;
	private static $protectedFolders;
	public static $taxonomyProtectionLevel;
	public static $debugmode;
	public static $themeFolder;
	public static $csfiltersTitle;
	public static $csfiltersAllLabel;

	public static $blockOptsSuperblock;
	public static $blockOptsPrefix;

	public static $forceResync;

	private static $relevantOptionsForSync;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->plugin_name = $plugin_name;
		$this->version = $version;
    	/*
		$this->jsCallbackManager = new ROSFJsCallbackManager("admin");
    	$this->registerCallbacks();
		*/
	  	self::$options_group = $this->plugin_name . '_rosf_opt';
	    self::$baseDir = self::$options_group . '_basedir';
	    self::$thumbsDir = self::$options_group . '_thumbsdir';
	    self::$newWhenYoungerThan = self::$options_group . '_nwyt';
		self::$subdirTaxonomies = self::$options_group . '_subtax';
		self::$maxMillisecondsPerStep = self::$options_group . '_mmsps';
		self::$protectedFolders = self::$options_group . '_protecteddirs';
		self::$taxonomyProtectionLevel = self::$options_group . '_taxprotlevel';
		self::$debugmode = self::$options_group . '_debugmode';
		self::$themeFolder = self::$options_group . '_themedir';
		self::$csfiltersTitle = self::$options_group.'_csftitle';
		self::$csfiltersAllLabel = self::$options_group.'_csallflabel';

		self::$blockOptsPrefix = self::$options_group . '_block';
		self::$blockOptsSuperblock = self::$options_group . '_superblock';
    
    	self::$plugin_options_slug = $this->plugin_name . '-admopt';

		
		if (empty(self::$relevantOptionsForSync)) {
			self::$relevantOptionsForSync = [self::$baseDir, self::$thumbsDir, self::$subdirTaxonomies, self::$forceResync];
			add_action('init', array($this, 'hookOptionsSave'));
		}
	}

	public function hookOptionsSave() {
		add_action( 'updated_option', array($this, 'updated'), 10, 3);
	}

	public function updated($option, $old_value, $new_value) {

		if ($option === self::$subdirTaxonomies) {
			ROSFSync::deleteOldTaxonomiesTerms($old_value, $new_value);
		}

		/*
		// registro la sync sullo shutdown, perché non ho trovato altro modo di eseguirla
		// una sola volta dopo che tutte le opzioni sono state salvate, ovvero pare che 
		// la settings API di WordPress non preveda un hook tipo settings_saved.
		// Gli hooks della option API vengono avviati una volta per ogni option e qui non 
		// è quel che mi serve.
		if (in_array($option, self::$relevantOptionsForSync)) {
			self::$relevantOptionsForSync = [];
			add_action( 'shutdown', array("ROSFSync", "sync"), 10, 0);
		}
		*/
	}
	 	
	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ro_Shared_Folders_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ro_Shared_Folders_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ro-shared-folders-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ro_Shared_Folders_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ro_Shared_Folders_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ro-shared-folders-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function register_settings() 
	{
	  register_setting(self::$plugin_options_slug, self::$baseDir);
	  register_setting(self::$plugin_options_slug, self::$thumbsDir);
	  register_setting(self::$plugin_options_slug, self::$newWhenYoungerThan);
	  register_setting(self::$plugin_options_slug, self::$subdirTaxonomies);
	  register_setting(self::$plugin_options_slug, self::$protectedFolders);
	  register_setting(self::$plugin_options_slug, self::$taxonomyProtectionLevel);
	  register_setting(self::$plugin_options_slug, self::$debugmode);
	  register_setting(self::$plugin_options_slug, self::$themeFolder);
	  register_setting(self::$plugin_options_slug, self::$csfiltersTitle);
	  register_setting(self::$plugin_options_slug, self::$csfiltersAllLabel);
	  
	  //register_setting(self::$plugin_options_slug, self::$forceResync);
	  add_settings_section(self::$plugin_options_slug, "Settings", array($this, "settings_section_title"), self::$plugin_options_slug);
	  add_settings_field(self::$baseDir, "Base directory", array($this, 'show_basedir_input'), self::$plugin_options_slug, self::$plugin_options_slug);
	  add_settings_field(self::$thumbsDir, "Thumbnails directory", array($this, 'show_thumbsdir_input'), self::$plugin_options_slug, self::$plugin_options_slug);
	  add_settings_field(self::$newWhenYoungerThan, "Days files remain new", array($this, 'show_NWYT_input'), self::$plugin_options_slug, self::$plugin_options_slug);
	  add_settings_field(self::$subdirTaxonomies, "Subdir taxonomies", array($this, 'show_subtax_input'), self::$plugin_options_slug, self::$plugin_options_slug);
	  add_settings_field(self::$protectedFolders, "Protected directories", array($this, 'show_protdir_input'), self::$plugin_options_slug, self::$plugin_options_slug);
	  add_settings_field(self::$taxonomyProtectionLevel, "Taxonomy protection level", array($this, 'show_taxprotlev_input'), self::$plugin_options_slug, self::$plugin_options_slug);
	  add_settings_field(self::$debugmode, "Debug mode", array($this, 'show_debug_check'), self::$plugin_options_slug, self::$plugin_options_slug);
	  add_settings_field(self::$themeFolder, "Theme folder", array($this, 'show_theme_folder_input'), self::$plugin_options_slug, self::$plugin_options_slug);
	  add_settings_field(self::$csfiltersTitle, "Case studies box label text", array($this, 'show_csftitle_input'), self::$plugin_options_slug, self::$plugin_options_slug);
	  add_settings_field(self::$csfiltersAllLabel, "Case studies \"View All\" label text", array($this, 'show_csallflabel_input'), self::$plugin_options_slug, self::$plugin_options_slug);
	}

	public function menu() 
	{
	  add_options_page(self::$plugin_options_slug, 'RO Shared Folders', 'manage_options', self::$plugin_options_slug, array($this, 'show_options'));
	}
	
	public function settings_section_title() 
	{
	  echo "<h3>".__("RO Shared Folders settings", "ro-shared-folders")."</h3>";
	}
  
	public function show_options() {
	  ?>
	  <div class="wrap">
		<h2>RO Shared Folders</h2>
		<form method="post" action="options.php">
	  <?php
	  settings_fields(self::$plugin_options_slug);
	  do_settings_sections(self::$plugin_options_slug);
	  submit_button();
	  echo "</form></div>";
	}

	private function resyncMessage() {
	  $stepsToGo = ROSFUpdatesQueue::getProgress();
	  if ($stepsToGo > 0) {
		echo '<div class="resyncmsg">A resync operation is already running in the background. There are '.$stepsToGo.' operations still queued.</div>';
	  }
	  return $stepsToGo > 0;
	}
	
	public function option($option_name, $default, $allowZero = false)
	{
	  $val = get_option($option_name, false);
	  if (empty($val) && (($allowZero === false && trim($val) === "0") || trim($val) === ""))
	  {
		update_option($option_name, $default);
		return $default;
	  }
	  return $val;
	}
	
	public function show_input_check($option_name, $default = false, $forceFalse = false)
	{
	  $v = $this->option($option_name, $default);
	  if (empty($v) || $forceFalse)
		$v = '';
	  else
		$v = 'checked="checked"';
	  ?>
	  <input type="checkbox" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" <?php echo $v; ?> />
	  <?php
	}
  
	public function show_input_type($type, $option_name, $default = "", $note = "", $allowZero = false)
	{
		?>
		<input type="<?php echo $type; ?>" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" size="20" value="<?php echo $this->option($option_name, $default, $allowZero); ?>"/>
		<?php if (!empty($note)) echo '<span class="rosf-admin-options-note rosf-admin-options-note-'.$option_name.'">'.$note.'</span>';
	}

	public function show_input_text($option_name, $default = "", $note = "", $allowZero = false)
	{
	  $this->show_input_type("text", $option_name, $default, $note, $allowZero);
	}
  
	public function show_input_number($option_name, $default = 0, $note = "")
	{
	  $this->show_input_type("number", $option_name, $default, $note, true);
	}

	public function show_select_option($option, $key, $value = NULL)
	{
	  echo '<option value="'.$key.'"';
	  $dbtext = get_option( $option, '');
	  if ($key == $dbtext)
			  echo " selected";
	  if (is_null($value))
		echo '>'.$key.'</option>';
	  else
		echo '>'.$value.'</option>';
	}
  
	public function show_basedir_input()
	{
	  $this->show_input_text(self::$baseDir, "", "The base directory for the reserved area files");    
	}
	  
	public function show_theme_folder_input()
	{
	  $this->show_input_text(self::$themeFolder, "", "A folder containing custom theme files (optional). If set, it must be the absolute filesystem path and it must be browsable under the /wp-content Wordpress URL.");    
	}
	
	public function show_csftitle_input()
	{
	  $this->show_input_text(self::$csfiltersTitle, "Filters", "Text label for the Case Studies Filters box on the front end ([rosf_cs_filters] shortcode)");    
	}
	
	public function show_csallflabel_input()
	{
	  $this->show_input_text(self::$csfiltersAllLabel, "View all", "Text label for the Case Studies \"View all\" filter radio button");    
	}

	public function show_thumbsdir_input()
	{
	  $this->show_input_text(self::$thumbsDir, "", "A writable directory for the thumbnails. Do NOT choose a parent of, nor the same as, nor a subdir of the base directory.");    
	}
	
	public function show_NWYT_input()
	{
	  $this->show_input_number(self::$newWhenYoungerThan, 30, "No. of days recently updated directories are shown as \"NEW\"");    
	}

	public function show_mmsps_input()
	{
	  $this->show_input_number(self::$maxMillisecondsPerStep, 200, "Max no. of millis used on each request to serve the capabilites updates queue (useful when the shared folder has many subdirectories");    
	}

	public function show_subtax_input()
	{
	  $this->show_input_text(self::$subdirTaxonomies, "Divisione,Brand,Tecnologia,Tipologia", "Taxonomies to create, one per subdir level, comma separated");    
	}

	public function show_protdir_input()
	{
	  $this->show_input_text(self::$protectedFolders, "Listini", "Folders names that need a separate capability (out of the capabilites tree) to be accessed, regardless of the permissions on the parents. Comma separated list.");    
	}
	
	public function show_taxprotlev_input()
	{
	  $this->show_input_text(self::$taxonomyProtectionLevel, "0 (none), 1 (first taxonomy), ...", "Generate roles for protected folders specific to this taxonomy");    
	}

	public function show_debug_check()
	{
	  $this->show_input_check(self::$debugmode);
	}

	public function show_resync_check()
	{
	  if (!$this->resyncMessage()) {
		$this->show_input_check(self::$forceResync, false, true);
	  }
	}

	public static function getOptionsSlug() {
		return self::$plugin_options_slug;
	}
    
	public static function getTaxonomyNames() {
		$cslist = get_option(self::$subdirTaxonomies, "");
		return explode(",", $cslist);
	}
	
	public static function getProtectedFolders() {
		$pflist = strtolower(get_option(self::$protectedFolders, ""));
		if (empty($pflist))
		  return [];
		return explode(",", $pflist);
	}
  }
