<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Ro_Shared_Folders
 * @subpackage Ro_Shared_Folders/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
