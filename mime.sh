#!/bin/bash

cat /etc/mime.types | tr '\t' ' ' | tr -s ' ' | sed -e 's/$/ /g' | grep ' exe ' | cut -d' ' -f1
