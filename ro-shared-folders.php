<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://virtualbit.it/
 * @since             1.0.0
 * @package           Ro_Shared_Folders
 *
 * @wordpress-plugin
 * Plugin Name:       RO Shared Folders
 * Plugin URI:        https://gitlab.com/lucrus/ro-shared-folders
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.1.240602
 * Author:            Virtual Bit
 * Author URI:        https://virtualbit.it/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ro-shared-folders
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'RO_SHARED_FOLDERS_VERSION', '1.0.0' );


global $rosf_basedir;
$rosf_basedir = plugin_dir_path(__FILE__);

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ro-shared-folders-activator.php
 */
function activate_ro_shared_folders() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ro-shared-folders-activator.php';
	Ro_Shared_Folders_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ro-shared-folders-deactivator.php
 */
function deactivate_ro_shared_folders() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ro-shared-folders-deactivator.php';
	Ro_Shared_Folders_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ro_shared_folders' );
register_deactivation_hook( __FILE__, 'deactivate_ro_shared_folders' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ro-shared-folders.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ro_shared_folders() {
	global $rosf_basedir;
	$plugin = new Ro_Shared_Folders($rosf_basedir);
	$plugin->run();

}
run_ro_shared_folders();
