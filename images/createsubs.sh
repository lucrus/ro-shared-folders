#!/bin/bash

TFILE=`mktemp`

curl http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types 2>/dev/null | sed -e 's/^#//' -e 's/^ //' | tr '\t' ' ' > "$TFILE"

SEPARATOR=`grep -n ==== "$TFILE" | cut -d: -f1`

mkdir mime

cd mime

cat "$TFILE" | tail -n +$((SEPARATOR + 1)) | cut -d' ' -f1 | xargs -n 1 mkdir -p 

mkdir ../filesext

cd ../filesext

cat "$TFILE" | tail -n +$((SEPARATOR + 1)) | cut -d' ' -s -f2- | xargs -n 1 mkdir -p

rm -f "$TFILE"

