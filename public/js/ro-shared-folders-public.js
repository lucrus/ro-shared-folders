(function( $ ) {
  'use strict';
  var restRoute;
  var restNamespace;
  var wpnonce;
  var currentResultsPage = 0;

	function toggleDirectory(dirNameElement) {
    var target = $(dirNameElement);
    if (typeof target === 'undefined' || target === null || target === false) {
      return;
    }
	  $(".rosf-current-directory").removeClass("rosf-current-directory");
	  target.addClass("rosf-current-directory");

    $(".rosf-permalink").unbind('click').click( function (ev) {
      ev.preventDefault();
      ev.stopPropagation();
      shareLink($(this).data("permalink"));
    });


    var depth = parseInt(target.data('rosf-depth')) + 1;
	  var containerId = target.data('rosf-contained-in');
	  var containedDirectoriesSelector = '[data-rosf-id="' + containerId + '"] .rosf-directory[data-rosf-depth="' + depth + '"]';
	  $(containedDirectoriesSelector).slideToggle("slow");
	  $('.rosf-directory-icon[data-rosf-id="' + containerId + '"').toggle();
    populateFilesList(target.data("rosf-id"));
    localStorage.setItem("rosf-current-directory", target.data("rosf-id"));
	}

  function showLocallyStoredDirectory() {
    var locallys = localStorage.getItem("rosf-current-directory");
    if (locallys) {
      // toggleDirectory(locallys);
    }
  }

  function shareLink(link) {

    $('#rosf-link-target-container').val(link);
    $('.rosf-sharing-lightbox').fadeIn(500);
    $('.rosf-sharing-lightbox').unbind('click').click(function () {
      $(this).fadeOut(500);
    });

    $('.rosf-sharing-dialog-content').unbind('click').click(function (ev) {
      ev.preventDefault();
      ev.stopPropagation();
    });

    $('.rosf-copytoclipboard').unbind('click').click(function () {
      // Get the text field
      var copyText = document.getElementById("rosf-link-target-container");

      // Select the text field
      copyText.select();
      copyText.setSelectionRange(0, 99999); // For mobile devices

      // Copy the text inside the text field
      navigator.clipboard.writeText(copyText.value);

      $("#rosf-copied-notification").fadeIn(300, function () {
        $("#rosf-copied-notification").fadeOut(700);
      });
    });
  }


	function closeAllNotFirstLevelDirectories() {
		$(".rosf-directory").not( "[data-rosf-depth='0']").hide();
		$(".rosf-open-directory-icon").hide();

		$(".rosf-directory-name").unbind('click').click( function (ev) { 
      toggleDirectory(this); }
    );

    showLocallyStoredDirectory();

  }

  function scrollToElement(jqBlock) {
    jqBlock.scrollParent().animate({
      scrollTop: jqBlock.offset().top
    }, 1000);    
  }

  function search(rosfSelect) {
    var filters = [];
    $(".rosf-select").each(function (index, select) {
      var filterIndex = parseInt($(select).data("depth"));
      var filterValue = $(select).val();
      if (!Array.isArray(filterValue)) filterValue = [filterValue];
      filters[filterIndex] = filterValue;
      }).promise().done( function() { 
        callWP('search', {'rosf_flt': filters, 'rosf_fnflt': $('#rosf-filenamefilter').val(), 'rosf_page': currentResultsPage}, function(results) {
          var target = $(rosfSelect).data("target");
          var newblock = $(results.markup).appendTo("." + target);
          newblock.slideToggle(500, () => { scrollToElement(newblock); });
          if (results.loadmore) {
            $(".rosf-loadmore").show();
          } else {
            $(".rosf-loadmore").hide();
          }
        });
      });  
  }

  function clearResults() {
    
    $(".rosf-selectmenu-results-panel").empty();
    $(".rosf-loadmore").hide();
    currentResultsPage = 0;
  }

  function manageSelectFilters() {
    var timer = undefined;
    
    $(".rosf-select").select2();
    $(".rosf-select").on('select2:select', function (_e) {
      if (timer) {
        clearTimeout(timer);
      }
      timer = undefined;
      clearResults();
      search(this); // e.params.data
    });
    
    var oldvalue = $("#rosf-filenamefilter").val();
    $("#rosf-filenamefilter").keypress(function (_e) {
      if (timer) {
        clearTimeout(timer);
      }
      var that = this;
      timer = setTimeout(function () { 
        timer = undefined;
        var newval = $("#rosf-filenamefilter").val();
        if (newval != oldvalue && newval.length > 2) {
          oldvalue = newval;
          clearResults(); 
          search(that);
        }
      }, 2000);
    });

    $(".rosf-loadmore").on("click", function () {
      currentResultsPage++;
      search(this);
    });

    $('#rosf-resetfilters').click(function  () {
      var fselect = ".rosf-select:not(.rosf-hidden-select)"; 
      $(fselect).val('*');
      $(fselect).trigger('change');
      $("#rosf-filenamefilter").val('');
      if (timer) {
        clearTimeout(timer);
      }
      timer = undefined;
      clearResults();
    });
  }

	function populateFilesList(rosfId) {
    callWP('listFiles', {'rosfId': rosfId}, function (results) {
      $(results.target).html(results.markup);       
     });
  }

  function setupDefaults(jqThis, event, argsarray)
  {
    restRoute = argsarray[0];
    restNamespace = argsarray[1];
    wpnonce = argsarray[2];
  }

  function redirectTo(jqThis, event, argsarray) {
    var autoredir_id = argsarray[0];
    var redirurl = argsarray[1];
    if (redirurl && redirurl.length > 0) {
      var autoredir_elem = document.getElementById(autoredir_id);
      if (autoredir_elem !== null) {
        window.location.href = redirurl;
      }
    }
  }

	function callWP(ajaxfn, params, responsefn) {
    $("#rosf-loading").show();
    var endpoint = restRoute + restNamespace + '/' + ajaxfn;
    var url = new URL(endpoint);
    url.search = new URLSearchParams(params).toString();
    
    fetch(url, {
      method : 'get',
      mode : 'cors',
      headers : {
        'Access-Control-Allow-Origin' : '*',
        'X-WP-Nonce' : wpnonce 
      }
    })
    .then(
      (response) => {
        console.log(response.json().then((data) => { 
          $("#rosf-loading").hide();
          responsefn(data);
      }))}).catch((err) => {
        console.error(err);
        $("#rosf-loading").hide();
      });
  }

    function parseEventsMappings(events)
    {
      $.each(events, function (index, value)
      {
        var target = value[0];
        if (target === 'public')
        {
          var event = value[1];
          var fname = value[2];
          var fargs = value[3];
          if (event === 'load')
            eval(fname + '($(this), null, fargs);');
          else
          {
            var cssclasstoselect = fargs[0];
            $('.' + cssclasstoselect).on(event, function (e) {
              return eval(fname + '($(this), e, fargs);');
            });
          }
        }
      });
    }

	$(document).ready(() => {

    if (!alreadyLoaded) {

      $(document).ajaxStart(function () {
        $('body').css({'cursor': 'wait'});
      }).ajaxStop(function () {
        $('body').css({'cursor': 'default'});
      });  
  
      parseEventsMappings(rofshooks_params.events);		
      closeAllNotFirstLevelDirectories();	 
      manageSelectFilters();  
    }
  
	});

  function checkScriptAlreadyIncluded() {
    var allTheScripts = $('script');
    var me = allTheScripts.last();
    var mySrc = me.attr("src");
    mySrc = mySrc.split("?")[0];
    allTheScripts.each(function (_idx, domscript) {
      var script = $(domscript)[0];
      var scriptSrc = script.src;
      scriptSrc = scriptSrc.split("?")[0];
      alreadyLoaded = alreadyLoaded || (script !== me && mySrc === scriptSrc);
    });
  }

  var alreadyLoaded = false;
  checkScriptAlreadyIncluded();

})( jQuery );
